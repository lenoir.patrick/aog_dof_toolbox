# doc.py
> Fonction de création des fichiers pydoc
```python
def recursiv_rep(dir, subdir):
    """Création des fichiers via pydoc
    
    :param str dir: répertoire de traitement
    :param str subdir: Répertoire de dépot des fichiers créés
    """
```

# hcstools.py
```python
def lecture_tab(opWD, key, logwrite=None):
    """
    Recuperation des parametres du modele
    
    :param str opWD: Chemin d'accès à l'op_wd
    :param str key: Entrée recherché dans le fichier applis.tab
    :param logwrite logwrite: Fichier de log
    """
```
```python
def run_command(commandline):
    """
    Fonction de lancement d'une commande
    
    :param str commandeline: Commande à executer
    """
```
```python
def get_nb_pages_ind(pathfile):
    """ Retourne le nombre de pages (lignes) dans le fichier .ind
    
    :param str pathfile: Chemin d'accès au fichier .ini
    
    :returns: Nombre de lignes contenues dans le fichier
    :rtype int:
    """
```
```python
class IndFile():
    def __init__(self, appname, logwrite, indexpage=False, compoHCS=False):
        """Cosntructeur de la classe de génération de fichier d'index HCS

        :param str appname: Nom de l'application
        :param logwrite logwrite: Fichier de log
        :param bool indexpage: Utilisation d'un index à la page (et non au pli)
        :param bool compoHCS: Utilsation d'une composition HCS (NOT USED)
        """

    def close(self):
        """Fermeture du fichier"""

    def get_hc_sindexfile(self, IndFile):
        """Lecture du fichier d'entrée
        
        :param str IndFile: Chemin d'accès au fichier"""

    def writelinepli(self, numDocLot, numPageDoc, line):
        """Ecriture d'un pli dans le fichier de sortie
        
        :param int numDocLot: Numéro du lot en cours
        :param int numPageDoc: Numéro de la page dans le lot
        :param str line: Ligne à écrire        
        """

    def writelinepage(self, numDocLot, numPageDoc, line):
        """Ecriture d'un page dans le fichier de sortie

        :param int numDocLot: Numéro du lot en cours
        :param int numPageDoc: Numéro de la page dans le lot
        :param str line: Ligne à écrire        
        """

    def writelinesdoc(self, nbPagesDoc, numDocLot, infos):
        """Ecriture des lignes d'un document

        :param int numDocLot: Numéro du lot en cours
        :param int num_page_doc: Numéro de la page dans le lot
        :param str infos: Informations à écrire        
        """

    def init_infos(self):
        """Intialisation des informations à écrire
        
        :returns: Les informations par défaut
        :rtype: dict
        """

    def init_files(self):
        """Initialisation des fichiers à écrire

        :returns: Les informations par défaut
        :rtype: dict
        """
```
```python
class PndFile():
    def __init__(self, appname, logwrite):
        """Constructeur de la classe d'index pour les documents PND
        
        :param str appname: Nom de l'application en cours
        :param logwrite logwrite: Fichier de log
        """
        
    def init_infos(self):
        """Intialisation des informations à écrire

        :returns: Les informations par défaut
        :rtype: dict
        """

    def init_files(self):
        """Initialisation des fichiers à écrire

        :returns: Les informations par défaut
        :rtype: dict
        """
        
    def writelinepli(self, line):
        """Ecriture d'un pli dans le fichier de sortie

        :param str line: Ligne à écrire        
        """
```

# logfile.py
```python
class Logfile:
    def __init__(self, name=None):
        """Constructeur de la classe de log
        
        :param str name: Nom de l'application utilisée
        """
```
```python
    def get_log_path(self):
        """Retourne la localisation du fichier de log
        
        :returns: Fichier de log
        :rtype str:
        """
```
```python
    def delete_old_logs(self, _log):
        """Supprimer les anciennes occurences de log
        
        :param logwrite _log: Nouvelle instance de log"""
```
```python
    def create_logger(self, level='info', console=True, debugparams={}):
        """Création de l'instance de logger
        
        :param str level: Niveau d'information demandé
        :param bool console: Affichage dans la console
        :param array debugparams: Paramètres du debugger
        """
```

# logvolumetries.py
```python
class Volumetries:
    def __init__(self, onglet, logwrite=None, year=None, env=False, generic=False):
        """
        Constructeur de la classe pour logguer les volumétries exploitées dans les traitements éditiques.

        :param str onglet: Nom de l'onglet à utiliser
        :param logwrite logwrite: Pointeur vers le fichier de log
        :param int year: Année de traitement
        :param bool env: Environnement de travail
        :param bool generic: Choix du fichier sur lequel travailler

        :raises Exception: Aucun logwrite n'est utilisé
        """
```

```python
    def openxlsxfile(self):
        """ Ouverture du fichier xlxs temporaire"""
```

```python
    def startarray(self, col, row, year):
        """Positionnement dans la bonne cellule du fichier

        :param int col: Numéro de colonne
        :param inf row: Numéro de ligne
        :param int year: Année utilisée pour un décalage
        """
```

```python
    def setvalue(self, year, month, day, nbenvoisposte):
        """Ecriture dans la cellule déterminée

        :param int year: Année en cours
        :param int month: Mois selectionné
        :param int day: Jour en cours
        :param int nbenvoisposte: Valeur à insérer dans la cellule
        """
```

```python
    def _savearray(self):
        """Enregistrement du fichier temporaire et chargement sur SharePoint"""
```

```python
    def closefile(self):
        """Fin du traitement et fermeture du fichier"""
```

# pdftools.py
```python
    def getnbpagespdf(pdf):
        """ Retourne le nombre de page d'un `pdf`.
        
        :param str pdf: Chemin d'accès vers le fichier
    
        :return: Le nom de page du fichier
        :rtype: int
        """
```
```python
    def add_datamatrix(pdffile, uuidval):
        """ Ajoute un code datamatrix de valeur `uuidval`au fichier `pdffile`.
        
        :param str uuidval: La valeur du code DataMatrix
        :param str pdffile: Chemin d'accès vers le fichier à modifier
        """
 ```
```python
    def contain_specific_word(pdf, word):
        """ Recherche dans le fichier `pdf` si l'occurence texte `word` est présente.
    
        :param tab pdf: Le fichier à tester
        :param str word: L'occurence texte à chercher
    
        :return: The word is found
        :rtype: bool
        """
```
```python
    def findspecificwordinpdf(files, word, logwrite):
        """ Recherche dans une liste de fichier `files` si l'occurence texte `word` est présente. Les informations sont 
        logguées dans le fichier `logwrite`. 
        
        :param tab files: La liste de fichiers à traiter
        :param str word: L'occurence texte à chercher
        :param logwrite logwrite: Pointeur vers le fichier de log
        """
```

# sendmailtols.py
> Methode standardisée d'envoi de mail de notificaiton sur les traitements. Elle utilise les classes standard `email`
> de python. Tout le paramétrage est géré dans cette classe et des méthodes sont présentes pour faciliter la création
> d'emails.
```python
class Sendmail():
    def __init__(self, config=None):
        """Constructeur de la classe Sendmail AssurOne Editique"""
```
```python
    def add_attachment(self, path):
        """Ajoute un fichier d'accompagnement au mail
        
        :param str path: Chemin d'accès au fichier
        """
```
```python
    def set_low_priority(self):
        """Définit une priorité basse au mail"""
```
```python
    def set_high_priority(self):
        """Définit une priorité haute au mail"""
```
```python
    def set_subject(self, subject, externe=False):
        """Définit le sujet du mail à envoyer
        
        :param str subject: Sujet du mail
        :param bool externe: Spécifique d'un mail vers l'extérieur à AOG"""
```
```python
    def set_body(self, body):
        """Définit le corps du mail
        
        :param str body: Corps du mail"""
```
```python
    def set_to(self, to):
        """Définit le destinataire principal
        
        :param str to"""
```
```python
    def set_cc(self, cc):
        """Définit le destinataire en copie

        :param str cc"""
```
```python
    def go_mail(self):
        """Envoie du mail"""
```

# sharepoint.py
```python
class Sharepoint:
    def __init__(self, logwrite):
        """Instanciation de la classe Sharepoint
        
        :param logwrite logwrite: Instance de log
        """
```    
```python
    def upload(self, remote_file, local_file=None, delete_input=True):
        """Charge le fichier sur Sharepoint
        
        :param str remote_file: Chemin vers le fichier à charger
        :param str local_file: Fichier local à charger
        :param bool delete_input: Supprimer le fichier local
        """
```
```python
    def download(self, remote_file):
        """Télécharger un fichier de Sharepoint
        
        :param str remote_file: Chemin du fichier à récupérer
        
        :return: Fichier local
        :rtype str:
```

# tools.py
> Méthodes génériques pour l'environnement de travail
```python
def get_time_stamp():
    """Retourne une date au format AAAAMMJJHHMMSS

    :returns: La date au fon format
    :rtype: time
    """
```

```python
def get_time_stamp_aaaammjj():
    """Retourne une date au format AAAAMMJJ

    :returns: La date au fon format
    :rtype: time
    """
```

```python
    """Créé un environnement de travail unique

    :param str appname: Nom de l'application utilisée

    :returns: Le chemin d'accès généré
    :rtype: str
    """
```

```python
    """Affiche le taux d'espace disponible sur le disque

    :param str disque: Répertoire à tester
    """
```
```python
    """Remplace une occurence texte par une autre

    :param str file: Fichier à modifier
    :param str old: Occurence à remplacer
    :param str new: Nouvelle occurence texte
    """
```
```python
def calc_jour_ouvre(jour, typetraitement, sstype, paramtab=None):
    """Calcule le prochain jour ouvré à partir du jour actuel

    :param datetime jour: Jour à tester
    :param str typetraitement: Type de traitement en cours pour du spécifique
    :param str sstype: Sous type de traitement
    :param str paramtab: Chemin d'accès vers une table de jours fériés

    :return: La nouvelle date d'envoie
    :rtype: datetime
    """
```

# transferttools.py
> Classes et méthode de transfert des fichiers.
```python
class UploadFtp():
    def __init__(self, inputdir, protocol=None, login=None, password=None, address=None, port=None, logwrite=None, debug=True, pathcmd=None):
        """Constructeur de la classe UploadFtp.

        :param str inputdir: Chemin d'accès au fichier à traiter
        :param str protocol: Protocole utilisé
        :param str login: Login utilisé
        :param str password: Password
        :param str address: Adresse ftp
        :param int port: port
        :param logwrite logwrite: Instance de log
        :param bool debug: Mode debug
        :param str pathcmd: Chemin d'accès au fichier de commande
        """
```
```python
    def setfiles(self, nbDocs, fileformat=".pdf", prefix=""):
        """Préparation des informations avant transfert (création fichier WinSCP, fichiers à transférer).
        
        :param int nbDocs: Nombre de fichies à traiter
        :param str fileformat: Format de fichiers à traiter
        :param str prefix: Préfixe des fichiers à traiter
        """
```
```python
    def uploadfiles(self, prod=True):
        """Transfert des fichiers
        
        :param bool prod: Mode production (active uniquement en production)
        """
```
