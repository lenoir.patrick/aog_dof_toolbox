#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
from PyPDF2 import PdfFileReader, PdfFileWriter
import re
import AOG.sendmailtools as sendmailtools
import json
import os
from pylibdmtx.pylibdmtx import encode
from PIL import Image
from reportlab.pdfgen import canvas

with open(os.path.join(os.path.dirname(__file__), 'config.json')) as json_data:
    config = json.load(json_data)


def getnbpagespdf(pdf) -> int:
    """ Retourne le nombre de pages d'un `pdf`.

        pdf: Chemin d'accès vers le fichier

    Returns:
        Le nom de pages du fichier
    """
    try:
        with open(pdf, 'rb') as f:
            pdf_doc = PdfFileReader(f)
            number_of_pages = pdf_doc.getNumPages()
            return number_of_pages
    except (Exception, ):
        return 0


def add_datamatrix(pdffile, uuidval):
    """ Ajoute un code datamatrix de valeur `uuidval`au fichier `pdffile`.

    Args:
        pdffile: Chemin d'accès vers le fichier à modifier
        uuidval: La valeur du code DataMatrix
    """
    if config["usinecourrier"]["active"] is True:
        # print(uuidval)
        # https://stackoverflow.com/questions/2925484/place-image-over-pdf
        encoded = encode(uuidval.encode('utf8'))
        img = Image.frombytes('RGB', (encoded.width, encoded.height), encoded.pixels)
        width, height = img.size
        width = int(width / 4)
        height = int(height / 4)
        # print(height)
        im1 = img.resize((width, height))
        im1.save('datamatrix.png')
        # print(Image.open('dmtx.png'))

        c = canvas.Canvas('datamatrix.pdf')
        # Draw the image at x, y. I positioned the x,y to be where i like here
        # Le positionnement est en point PICA. L'origine est le point 0 en PostScript (en bas à droite de la page)
        c.drawImage('datamatrix.png', config["usinecourrier"]["datamatrix_x"], config["usinecourrier"]["datamatrix_y"])
        c.save()

        output_file = PdfFileWriter()

        pdf_original = pdffile
        pdf_datamatrix = pdffile + "_datamatrix.pdf"
        pdf_tmp = pdffile + "_tmp.pdf"

        with open("datamatrix.pdf", "rb") as f:
            watermark = PdfFileReader(f)
            with open(pdf_original, "rb") as f1:
                input_file = PdfFileReader(f1)

                # Number of pages in input document
                page_count = input_file.getNumPages()

                for page_number in range(page_count):
                    # merge the watermark with the page
                    input_page = input_file.getPage(page_number)
                    if page_number == 0:
                        input_page.mergePage(watermark.getPage(0))
                    # add page from input file to output document
                    output_file.addPage(input_page)

                with open(pdf_datamatrix, "wb") as output_stream:
                    output_file.write(output_stream)

        os.rename(pdf_original, pdf_tmp)
        os.rename(pdf_datamatrix, pdf_original)
        os.remove(pdf_tmp)
        os.remove("datamatrix.pdf")
        os.remove("datamatrix.png")


def contain_specific_word(pdf, word) -> bool:
    """Recherche dans le fichier `pdf` si l'occurence texte `word` est présente.

    Args:
        pdf: Le fichier à tester
        :param str word: L'occurence texte à chercher

    Returns:
        The word is found
    """
    try:
        with open(pdf, 'rb') as f:
            pdf_doc = PdfFileReader(f)
            for i in range(0, pdf_doc.getNumPages()):
                page_obj = pdf_doc.getPage(i)
                text = page_obj.extractText()
                if re.search(word, text):
                    return True
        return False
    except Exception as ex:
        print(str(ex))
        return False


def findspecificwordinpdf(files, word, logwrite):
    """ Recherche dans une liste de fichier `files` si l'occurence texte word est présente. Les informations sont
    logguées dans le fichier logwrite

    Args:
        files: La liste de fichiers à traiter
        word: L'occurence texte à chercher
        logwrite: Le pointeur vers le fichier de log
    """
    logwrite.info("Récupération des docs avec " + word)
    mailstgratien = sendmailtools.Sendmail()
    mailstgratien.set_subject("Document contenant " + word + " à modifier")
    mailstgratien.set_high_priority()
    nb = 0
    for file in files:
        if file.endswith(".pdf"):
            trouve = contain_specific_word(file, word)
            if trouve is True:
                nb = nb + 1
                mailstgratien.add_attachment(file)
                logwrite.warning("     Template courrier à modifier : " + os.path.basename(file))
    if nb > 0:
        mailstgratien.go_mail()


if __name__ == "__main__":
    import AOG.tools as aogtools
    appname = os.path.basename(__file__.upper())
    gc = aogtools.GeneralConfig(appname)

    pdf_name = os.path.join(os.path.dirname(__file__), "loremipsum.pdf")
    gc.logwrite.info("PDF de test : " + pdf_name)

    gc.logwrite.info("Nombre de page du pdf : " + str(getnbpagespdf(pdf_name)))

    gc.logwrite.info("contient lorem : " + str(contain_specific_word(pdf_name, "lorem")))
    gc.logwrite.info("contient test : " + str(contain_specific_word(pdf_name, "test")))

    shutil.copy(pdf_name, pdf_name + "_dm.pdf")

