DROP TABLE IF EXISTS `usine_courriers_editique`.`usine_courrier`;
DROP TABLE IF EXISTS `usine_courriers_editique`.`production_papier`;

CREATE TABLE IF NOT EXISTS `usine_courriers_editique`.`production_papier` (
  `ID` VARCHAR(40) NOT NULL,
  `type` VARCHAR(10) NULL,
  `nbdocs` INT NULL,
  `nbpages` INT NULL,
  `DateTraitement` DATE NULL,
  `DateImpression` DATE NULL,
  `duree` VARCHAR(45) NULL,
  `cout_total` DECIMAL(10,2) NULL,
  `acteurs_Nom` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `fk_production_papier_acteurs1_idx` (`acteurs_Nom` ASC) VISIBLE,
  CONSTRAINT `fk_production_papier_acteurs1`
    FOREIGN KEY (`acteurs_Nom`)
    REFERENCES `usine_courriers_editique`.`partenaires` (`Nom`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `usine_courriers_editique`.`usine_courrier` (
  `NomFichierEntree` VARCHAR(100) NULL,
  `NomFichierSortie` VARCHAR(100) NULL,
  `DateTraitement` DATE NULL,
  `DateImpression` DATE NULL,
  `ReferenceContrat` VARCHAR(45) NULL,
  `Branche` VARCHAR(45) NULL,
  `Produit` VARCHAR(45) NULL,
  `Marque` VARCHAR(45) NULL,
  `Compagnie` VARCHAR(45) NULL,
  `Courtier` VARCHAR(45) NULL,
  `TypeDocCortex` VARCHAR(100) NULL,
  `Lotissement` VARCHAR(45) NULL,
  `idLot` VARCHAR(45) NULL,
  `canalDistribution` VARCHAR(45) NULL,
  `refDistribution` VARCHAR(15) NOT NULL,
  `TypeImpression` VARCHAR(45) NULL,
  `TypePage` VARCHAR(45) NULL,
  `TypePapier` VARCHAR(45) NULL,
  `TypeEnveloppe` VARCHAR(45) NULL,
  `nbpages` INT NULL,
  `nbfeuilles` INT NULL,
  `refPV` VARCHAR(45) NULL,
  `refConstat` VARCHAR(45) NULL,
  `uuidpli` VARCHAR(100) NOT NULL,
  `pdspapier` DECIMAL(5,2) NULL,
  `pxpapier` DECIMAL(5,4) NULL,
  `pdsenv` DECIMAL(5,2) NULL,
  `pxenv` DECIMAL(5,4) NULL,
  `pdsconstat` DECIMAL(5,2) NULL,
  `pxconstat` DECIMAL(5,4) NULL,
  `pdspv` DECIMAL(5,2) NULL,
  `pxpv` DECIMAL(5,4) NULL,
  `pxtraitement` DECIMAL(5,4) NULL,
  `pxaffranchissement` DECIMAL(5,4) NULL,
  `production_papier_ID` VARCHAR(40) NOT NULL,
  `recommande` VARCHAR(45) NULL,
  `pnd` TINYINT NULL,
  PRIMARY KEY (`uuidpli`),
  INDEX `fk_usine_courrier_acteurs1_idx` (`refDistribution` ASC) VISIBLE,
  INDEX `fk_usine_courrier_production_papier1_idx` (`production_papier_ID` ASC) VISIBLE,
  CONSTRAINT `fk_usine_courrier_acteurs1`
    FOREIGN KEY (`refDistribution`)
    REFERENCES `usine_courriers_editique`.`partenaires` (`Nom`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usine_courrier_production_papier1`
    FOREIGN KEY (`production_papier_ID`)
    REFERENCES `usine_courriers_editique`.`production_papier` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB

