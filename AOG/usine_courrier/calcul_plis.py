# coding: utf-8
import json
import pandas
import os
from os import listdir
from os.path import isfile, join
import AOG.usine_courrier.usine_courrier as usinecourrier
import AOG.tools as tools
import AOG.logfile as logfile
import shutil
import sys

with open(os.path.join(os.path.dirname(__file__), "..", 'config.json')) as json_data:
    config = json.load(json_data)

with open(os.path.join(os.path.dirname(__file__), 'config_pli.json')) as json_data:
    configplis = json.load(json_data)


class CalculPlis:
    def __init__(self, logwrite):
        self.logwrite = logwrite

    def parcours_dir(self, dir):
        for file in listdir(dir):
            openfile = join(dir, file)

            if isfile(openfile):
                # self.logwrite.info("########## Nouveau fichier ##########")
                self.traite_lot(dir, openfile)

    def traite_lot(self, dir, openfile):
        file = os.path.basename(openfile)
        if file.endswith(".ind"):
            self.logwrite.info("Usine Courrier - Traitement du fichier " + file)
            df = pandas.read_csv(openfile, encoding='latin-1', sep="\t")
            # df.sort_values(by=['ReferenceContrat'], inplace=True)
            self.logwrite.info("    Ouverture du fichier ok")

            nomprestataire = file.split("_")[2].upper()
            section = file.split("_")[3].upper()

            if len(df) > 0:
                data_frame, data_frame_copy, cout_surtaxe = self.calcule_pli(df, file)
                # print(data_frame['nbPages'])
                self.logwrite.info("    Fin du traitement DataFrame")

                # nomprestataire est index 2 et section index 3
                sous_type_prod = file.split("_")[4].upper()

                try:
                    # Mise à jour du Dataframe
                    outfile = join(dir, file + ".csv")

                    """ TODO : Quite a little bit tricky. On recherche la première occurence de la copie, mais comme
                    il s'agit d'une copie dans laquelle on a supprimé des lignes, la première ligne n'est
                    pas forcemment exploitable.
                    """
                    fichiervide = True
                    idx_data_frame = 0
                    datetraitement = ""
                    while fichiervide is True:
                        try:
                            datetraitement = str(data_frame_copy['DateTraitement'][idx_data_frame])
                            fichiervide = False
                        except (Exception,):
                            pass
                        idx_data_frame = idx_data_frame + 1
                    datetraitement = datetraitement.replace("-", "")
                    datetraitement = datetraitement.replace("/", "")
                    dateimpression, add_cout_traitement = tools.calc_jour_ouvre(datetraitement, section, sous_type_prod)

                    if dateimpression != datetraitement:
                        self.logwrite.info("    La date d'impression calculée est le : " + str(dateimpression))
                    self.logwrite.info("    Calcul des infos du lot de production")
                    cout_plis = data_frame_copy['pxtotal'].sum()
                    cout_aff = data_frame_copy['pxaffranchissement'].sum()
                    self.logwrite.info("    Total affranchissement : " + str(cout_aff) + "€")
                    cout_aff = cout_aff + cout_surtaxe
                    self.logwrite.info("    Total affranchissement avec surtaxe : " + str(cout_aff) + "€")

                    cout_traitement = 0

                    nb_plis = len(data_frame_copy)
                    nb_pages = data_frame_copy['nbPages'].sum()

                    sectionjson = nomprestataire + "_" + section
                    if sectionjson not in configplis:
                        sectionjson = nomprestataire

                    # Une facturation par traitement est appliquée.
                    self.logwrite.info("    Récupération des infos de traitement du lot")

                    if "PRIX_TRT" in configplis[sectionjson]["PRESTATIONS"]:
                        # Si le total est inférieur à un certain montant, on applique un montant minimum de facturation
                        if section == "LC":
                            montant_minimum = configplis[sectionjson]["PRESTATIONS"]["PRIX_TRT"]
                            if montant_minimum > cout_plis:
                                cout_traitement = montant_minimum - cout_plis
                                # TODO : ENVOYER UNE ALERTE MAIL EN CAS DE SURFACTURATION POUR VOLUME INSUFFISANT
                        elif section == "LR":
                            # Si le traitement est un samedi ou dimanche, alors on ne compte le cout du traitement
                            # cas la prod sera prise en compte le jour suivant
                            # TODO : Gérer les jours fériés
                            # TODO : Gérer le retour COGEPRINT pour toper directement (plus de pb des jours fériés)
                            if add_cout_traitement is True:
                                cout_traitement = configplis[sectionjson]["PRESTATIONS"]["PRIX_TRT"]
                        else:

                            cout_traitement = configplis[sectionjson]["PRESTATIONS"]["PRIX_TRT"]
                    cout_traitement = cout_traitement * 1.2

                    if 'COGEPRINT_TERMES' in file:
                        self.logwrite.info("    Specif Termes")
                        file = "{0}_{1}_{2}_{3}_{4}_{5}*.ind".format(file.split("_")[0], file.split("_")[1],
                                                                     file.split("_")[2], file.split("_")[3],
                                                                     file.split("_")[4], file.split("_")[5][0:8])

                    usine_courrier = usinecourrier.UsineCourrier(logwrite)
                    self.logwrite.info("    Fin du travail préparatoire")
                    id_production = file.replace(".ind", "").replace("*", "")

                    self.logwrite.info("Vérification de l'éxistance de la production")
                    sql = "SELECT * FROM production_papier WHERE ID = '" + id_production + "';"
                    result = usine_courrier.select_sql(sql)

                    if len(result) == 0:
                        self.logwrite.info("Usine Courrier - Enregistrement de la prod : " + id_production)
                        sql = "INSERT INTO production_papier VALUES ('" + str(id_production) + "', '" + str(section) + "' , " + str(nb_plis) + ", " + str(nb_pages) + ", '" + str(datetraitement) + "', '" + str(dateimpression) + "', " + str(cout_plis) + ", " + str(cout_traitement) + ", " + str(cout_aff) + ", 0, 0, '" + nomprestataire + "', '" + sous_type_prod + "');"
                        usine_courrier.set_sql(sql)
                        self.logwrite.info("Usine Courrier - Enregistrement des données des plis")

                        if section != "LC":
                            data_frame['DateImpression'] = dateimpression

                        data_frame['production_papier_ID'] = id_production

                        data_frame.to_csv(outfile, index=False)
                        outfile = outfile.replace("\\", "\\\\")
                        outfile = outfile.replace("/", "\\\\")
                        usine_courrier.set_csv_file(outfile)
                        usine_courrier.commit()

                        self.logwrite.info("Enregistrement terminé")
                    else:
                        # Comme il y a plusieurs fichiers pour un run (et donc une production).
                        # On massifie les infos globales dans la base
                        if 'COGEPRINT_TERMES' in id_production:
                            self.logwrite.warning("La production Termes existe déjà, on mets à jour les compteurs")
                            for row in result:
                                nb_plis = int(row[3] + nb_plis)
                                nb_pages = int(row[2] + nb_pages)
                                cout_plis = float(row[6]) + float(cout_plis)
                                cout_aff = float(row[8]) + float(cout_aff)

                                sql = "UPDATE production_papier SET nbplis = \'{0}\', nbpages = \'{1}\',  cout_total " "= \'{2}\', cout_affranchissement = \'{3}\' where  ID = \'{4}\';".format(str(nb_plis), str(nb_pages), str(cout_plis), str(cout_aff), id_production)
                                usine_courrier.set_sql(sql)
                                self.logwrite.info("Usine Courrier - Mise à jour des données OK")

                                data_frame['production_papier_ID'] = id_production

                                data_frame.to_csv(outfile, index=False)
                                outfile = outfile.replace("\\", "\\\\")
                                outfile = outfile.replace("/", "\\\\")
                                usine_courrier.set_csv_file(outfile)

                                # On fait le commit SEULEMENT si tout a réussi.
                                usine_courrier.commit()
                                self.logwrite.info("Enregistrement terminé")
                            # sys.exit(0)
                        else:
                            data_frame['production_papier_ID'] = file
                            data_frame.to_csv(outfile, index=False)
                            self.logwrite.warning("Usine Courrier - Le fichier a déjà été traité, on passe au suivant")

                    try:
                        os.remove(outfile)
                    except (Exception,):
                        pass
                    outputdir = os.path.join(dir, nomprestataire, section)
                except Exception as ex:
                    self.logwrite.error("Err : " + str(ex))
                    self.logwrite.error('Erreur sur le traitement')
                    outputdir = os.path.join(dir, "ERREUR")
                    # sys.exit(0)
            else:
                self.logwrite.warning("    Le fichier est vide")
                outputdir = os.path.join(dir, nomprestataire, section, "VIDE")

            self.logwrite.info("    Archivage du fichier")
            os.makedirs(outputdir, exist_ok=True)
            shutil.copy(openfile, outputdir)
            os.remove(openfile)

        if file.endswith(".sql"):

            nomprestataire = file.split("_")[1].upper()
            section = file.split("_")[2].upper()

            self.logwrite.info("Usine Courrier - Traitement du fichier " + file)
            usine_courrier = usinecourrier.UsineCourrier(logwrite)
            try:
                usine_courrier.upload_sql_file(openfile)
            except (Exception, ):
                sys.exit(0)

            outputdir = os.path.join(dir, nomprestataire, section)
            self.logwrite.info("    Archivage du fichier")
            os.makedirs(outputdir, exist_ok=True)
            shutil.copy(openfile, outputdir)
            os.remove(openfile)

    # Calcule le prix de chaque pli. Il n'y pas d'agrégation
    def calcule_pli(self, df, filename):
        # Choix de la section à utiliser pour la tarification postale

        # https://stackoverflow.com/questions/49728421/pandas-dataframe-settingwithcopywarning-a-value-is-trying-to-be-set-on-a-copy
        pandas.options.mode.chained_assignment = None

        # Initialisation des champs supplémentaires
        df['pdspapier'] = 0.00
        df['pxpapier'] = 0.00
        df['pdenv'] = 0.00
        df['pxenv'] = 0.00
        df['pdconstat'] = 0.00
        df['pxconstat'] = 0.00
        df['pdpv'] = 0.00
        df['pxpv'] = 0.00
        df['pxtraitement'] = 0.00
        df['pxaffranchissement'] = 0.00
        df['production_papier_ID'] = ""
        df['recommande'] = ""
        df['pnd'] = 0
        df['pxpnd'] = 0

        nbplislot = len(df.index)
        typerun = filename.split("_")[3]
        sstype = filename.split("_")[4]

        self.logwrite.info("    Mise à jour unitaire des plis")
        section = ""

        for i in df.index:
            # print(df['ReferenceContrat'][i])
            section = filename.split("_")[3].upper()
            section = df['refDistribution'][i] + "_" + section

            try:
                configplis[section]
            except (Exception,):
                section = df['refDistribution'][i]
            try:
                if df['canalDistribution'][i] != "EMAIL":
                    # On passe le document à l'état d'envoyé sur un document imprimé.
                    df['pnd'] = 7
                    if df["TypePapier"][i] == "-":
                        if df["TypeDocCortex"][i] != "CVDEFINITIVE":
                            df["TypePapier"][i] = "PAPIER-BLANC"
                        else:
                            df["TypePapier"][i] = "PAPIER-CV"

                    if df['DateImpression'][i] != "--":
                        df['DateTraitement'][i] = df['DateImpression'][i]
                    else:
                        df['DateImpression'][i] = df['DateTraitement'][i]

                    # PAPIER
                    df['pdspapier'][i] = (df['nbFeuilles'][i].astype(int) * float(
                        configplis[section][df["TypePapier"][i]]["POIDS"]))
                    df['pxpapier'][i] = (df['nbFeuilles'][i].astype(int) * float(
                        configplis[section][df["TypePapier"][i]]["PUHT"]))

                    # ENVELOPPE PAR DEFAUT
                    df["TypeEnveloppe"][i] = configplis[section]['AFFRANCHISSEMENT']["ENV_STD"]
                    df['pdenv'][i] = float(configplis[section][df["TypeEnveloppe"][i]]["POIDS"])
                    df['pxenv'][i] = float(configplis[section][df["TypeEnveloppe"][i]]["PUHT"])

                    # CONSTAT
                    detail_pv = self.get_detail_consommable(df["refConstat"][i], section)
                    df['pxconstat'][i] = detail_pv['PRIX']
                    df['pdconstat'][i] = detail_pv['POIDS']

                    # PORTE-VIGNETTE
                    detail_pv = self.get_detail_consommable(df["refPV"][i], section)
                    df['pxpv'][i] = detail_pv['PRIX']
                    df['pdpv'][i] = detail_pv['POIDS']

                    #  print(data)
                    # print(df["TypePapier"][i])
                    type_impression = "PRIX_" + configplis[section][df["TypePapier"][i]]["TYPE"]
                    # print(type_impression)

                    # # AFFRANCHISSEMENT / MSP
                    df['pxtraitement'][i] = float(configplis[section]['PRESTATIONS']["PRIX_MSP"]) + float(
                        configplis[section]['PRESTATIONS'][type_impression]) * df['nbFeuilles'][i].astype(int)
                    if typerun == "LR":
                        # SI LE NB DE PLIS EST SUPERIEUR A 1000 LE COUT AU PLI EST INFERIEUR
                        if nbplislot < float(configplis[section]['AFFRANCHISSEMENT']["TRANCHE"]):
                            df['pxaffranchissement'][i] = float(configplis[section]['AFFRANCHISSEMENT']["PRIX_STD"])
                        else:
                            df['pxaffranchissement'][i] = float(configplis[section]['AFFRANCHISSEMENT']["PRIX_1000"])

                    elif typerun == "GESTION":
                        poidsenveloppe = df['pdpv'][i] + df['pdconstat'][i] + df['pdspapier'][i] + df['pdenv'][i]
                        detail_tarif_postal = self.get_detail_tarif_postal(df['ReferenceContrat'][i],
                                                                           poidsenveloppe, section)
                        df['pxaffranchissement'][i] = detail_tarif_postal['pxaffranchissement']
                        df['TypeEnveloppe'][i] = detail_tarif_postal['TypeEnveloppe']
                        df['pxenv'][i] = detail_tarif_postal['pxenv']
                        df['pdenv'][i] = detail_tarif_postal['pdenv']
                    else:
                        df['pxaffranchissement'][i] = float(configplis[section]['AFFRANCHISSEMENT']["PRIX_STD"])

            except KeyError as KE:
                self.logwrite.error("Usine Courrier - Erreur de configuration pour " + str(KE) +
                                    " dans la section : " + section)
                sys.exit(0)

        groupybycolumns = ['ReferenceContrat', 'refDistribution', 'canalDistribution']
        grouped = df.groupby(groupybycolumns)

        # occurence de chaque numero de contrat
        groupe_size = grouped.filter(lambda x: len(x) > 1)

        group_apply = (grouped.apply(lambda x: len(x) > 1))

        # Si pas d'id à regrouper ou termes AIG ⇨ Pas de regroupement de plis
        if groupe_size.empty or sstype == "AIG":
            self.logwrite.info("    Tous les numéros de contrat sont uniques")
        else:
            self.logwrite.info("    On est sur un traitement avec regroupement de plis")
            # Calcul de la somme de chaque ligne de chaque groupe
            ref_pv_df = df.groupby(groupybycolumns)['refPV'].apply(list).reset_index()
            ref_constat_df = df.groupby(groupybycolumns)['refConstat'].apply(list).reset_index()

            nbpagestotal = df.groupby(groupybycolumns)['nbPages'].sum().astype(int).tolist()
            nbfeuillestotal = df.groupby(groupybycolumns)['nbFeuilles'].sum().astype(int).tolist()

            pdstotalpapier = df.groupby(groupybycolumns)['pdspapier'].sum().astype(float).tolist()
            pxtotalpapier = df.groupby(groupybycolumns)['pxpapier'].sum().astype(float).tolist()

            pxtotaltraitement = df.groupby(groupybycolumns)['pxtraitement'].sum().astype(float).tolist()
            self.logwrite.info("    Fin des sommes intermédiaires")

            i = 0
            # Parcours de chaque groupe pour détermination des PV / Constats
            self.logwrite.info("    Regroupement des plis")
            for items, groupe in grouped:
                # pour chaque groupe regroupable
                nom = items[0]
                ref_distribution = items[1]

                # S'il y a regroupement, on modifie les caractéristiques des composantes du pli global

                if ref_distribution != "ASSURONE":
                    if group_apply[nom][ref_distribution][0]:
                        # Si pas de regroupement, on ne modifie pas le contenu du pli
                        self.logwrite.debug("    Regroupement du pli : " + nom)
                        list_pv = ref_pv_df["refPV"][i]
                        for item in list_pv:
                            if item is not None and item != "-":
                                detail_pv = self.get_detail_consommable(item, section)
                                df.loc[(df['ReferenceContrat'] == nom) & (
                                        df['refDistribution'] == ref_distribution), 'refPV'] = item
                                df.loc[(df['ReferenceContrat'] == nom) & (
                                        df['refDistribution'] == ref_distribution), 'pxpv'] = detail_pv['PRIX']
                                df.loc[(df['ReferenceContrat'] == nom) & (
                                        df['refDistribution'] == ref_distribution), 'pdpv'] = detail_pv['POIDS']
                                break

                        list_constat = ref_constat_df["refConstat"][i]
                        for item in list_constat:
                            if item is not None and item != "-":
                                detail_constat = self.get_detail_consommable(item, section)
                                df.loc[(df['ReferenceContrat'] == nom) & (
                                        df['refDistribution'] == ref_distribution), 'refConstat'] = item
                                df.loc[(df['ReferenceContrat'] == nom) & (
                                        df['refDistribution'] == ref_distribution), 'pxconstat'] = detail_constat[
                                    'PRIX']
                                df.loc[(df['ReferenceContrat'] == nom) & (
                                        df['refDistribution'] == ref_distribution), 'pdconstat'] = detail_constat[
                                    'POIDS']
                                break

                        df.loc[(df['ReferenceContrat'] == nom) & (
                                df['refDistribution'] == ref_distribution), 'nbPages'] = nbpagestotal[i]
                        df.loc[(df['ReferenceContrat'] == nom) & (
                                df['refDistribution'] == ref_distribution), 'nbFeuilles'] = nbfeuillestotal[i]

                        df.loc[(df['ReferenceContrat'] == nom) & (
                                df['refDistribution'] == ref_distribution), 'pdspapier'] = pdstotalpapier[i]
                        df.loc[(df['ReferenceContrat'] == nom) & (
                                df['refDistribution'] == ref_distribution), 'pxpapier'] = pxtotalpapier[i]

                        df.loc[(df['ReferenceContrat'] == nom) & (
                                df['refDistribution'] == ref_distribution), 'pxtraitement'] = pxtotaltraitement[i]
                else:
                    # Si pli vers Assurone (ie MAIL), on met les compteurs à 0.
                    df.loc[(df['ReferenceContrat'] == nom) & (df['refDistribution'] == ref_distribution), 'nbPages'] = 0
                    df.loc[
                        (df['ReferenceContrat'] == nom) & (df['refDistribution'] == ref_distribution), 'nbFeuilles'] = 0
                    df.loc[
                        (df['ReferenceContrat'] == nom) & (df['refDistribution'] == ref_distribution), 'pdspapier'] = 0
                    df.loc[(df['ReferenceContrat'] == nom) & (df['refDistribution'] == ref_distribution), 'pxpapier'] = 0
                    df.loc[(df['ReferenceContrat'] == nom) & (
                            df['refDistribution'] == ref_distribution), 'pxtraitement'] = 0
                # sys.exit()
                i = i + 1

            # On duplique le df pour calculer la tarification au pli en fonction du poids total de l'enveloppe
            self.logwrite.warning("Calcul du poids total des plis")
            df_copy = df.copy()
            df_copy.drop_duplicates(subset=groupybycolumns, keep='first', inplace=True)
            list_occ_pds = df_copy.groupby(groupybycolumns).agg(
                {'pdspapier': 'sum', 'pdenv': 'sum', 'pdconstat': 'sum', 'pdpv': 'sum'}).astype(float).sum(
                axis=1).tolist()

            i = 0
            # Parcours de chaque groupe
            self.logwrite.info("    Mise à jour du choix d'enveloppe et de la tarification")
            for items, groupe in grouped:
                # pour chaque groupe regroupable
                nom = items[0]
                ref_distribution = items[1]
                canal_distribution = items[2]

                if group_apply[nom][ref_distribution][0] is True and ref_distribution != "ASSURONE":
                    poidsenveloppe = list_occ_pds[i]

                    section = ref_distribution + "_" + canal_distribution
                    if section not in configplis:
                        section = ref_distribution

                    detail_tarif_postal = self.get_detail_tarif_postal(nom, poidsenveloppe, section)
                    df.loc[(df['ReferenceContrat'] == nom) & (
                            df['refDistribution'] == ref_distribution), 'pxaffranchissement'] = detail_tarif_postal[
                        'pxaffranchissement']
                    df.loc[(df['ReferenceContrat'] == nom) & (
                            df['refDistribution'] == ref_distribution), 'TypeEnveloppe'] = detail_tarif_postal[
                        'TypeEnveloppe']
                    df.loc[(df['ReferenceContrat'] == nom) & (df['refDistribution'] == ref_distribution), 'pxenv'] = \
                        detail_tarif_postal['pxenv']
                    df.loc[(df['ReferenceContrat'] == nom) & (df['refDistribution'] == ref_distribution), 'pdenv'] = \
                        detail_tarif_postal['pdenv']
                i = i + 1
            self.logwrite.info("    Fin de la mise à jour du choix d'enveloppe et de la tarification")

        self.logwrite.info("    Calcul des prix HT -> TTC (* 1.2)")
        df['pxpapier'] = df['pxpapier'] * 1.2
        df['pxenv'] = df['pxenv'] * 1.2
        df['pxconstat'] = df['pxconstat'] * 1.2
        df['pxpv'] = df['pxpv'] * 1.2
        df['pxtraitement'] = df['pxtraitement'] * 1.2

        self.logwrite.info("    Copie du data frame")
        df_copy = df.copy()
        df_copy.drop_duplicates(subset=groupybycolumns, keep='first', inplace=True)
        df_copy['pxtotal'] = df_copy['pxpapier'] + df_copy['pxenv'] + df_copy['pxconstat'] + df_copy['pxpv'] + df_copy[
            'pxtraitement']
        df_copy['pdstotal'] = df_copy['pdspapier'] + df_copy['pdenv'] + df_copy['pdconstat'] + df_copy['pdpv']

        df_copy = df_copy.loc[df_copy['canalDistribution'] != 'EMAIL']
        df_copy.reset_index(drop=True)

        # Calcul de la surtaxe au poids
        cout_surtaxe = 0
        if section == "PREVOIR":
            self.logwrite.info("    Traitement PREVOIR, on calcule si il y a une surtaxe au pli")
            # TODO : Reaffecter la surtaxe par pli (au poids ?)
            # Dans les envois LETTRE VERTE EN NOMBRE si le pli dépasse 35g, il y a une surtaxe de 10.80€ par kg entammé
            groupedsurtaxe = df_copy.groupby('pdstotal')

            limite_poids = configplis[section]['AFFRANCHISSEMENT']["LIMITE_POIDS"]

            surtaxe_kg = float(configplis[section]['AFFRANCHISSEMENT']["SURTAXE"])
            surtaxe_df = groupedsurtaxe.apply(lambda x: x[x['pdstotal'] > limite_poids]['pdstotal'].sum()).reset_index()
            surtaxe_df_exclusion = surtaxe_df[surtaxe_df[0] != 0.0]
            poidstotal = surtaxe_df_exclusion['pdstotal'].sum()

            nbkg = (poidstotal / 1000)
            self.logwrite.info("    Poids total soumis à surtaxe : " + str(nbkg) + "kg")
            # Arrondi au kg supérieur
            nbkg = round(nbkg + 0.5)

            cout_surtaxe = nbkg * surtaxe_kg
            self.logwrite.info("    Surtaxe totale : " + str(cout_surtaxe) + "€")

        return df, df_copy, cout_surtaxe

    def get_detail_tarif_postal(self, nom, poidsenveloppe, section):

        detail_tarif_postal = dict()
        detail_tarif_postal['pxaffranchissement'] = 0
        detail_tarif_postal['TypeEnveloppe'] = "-"
        detail_tarif_postal['pxenv'] = 0
        detail_tarif_postal['pdenv'] = 0
        try:
            poidsmax = float(configplis[section]['AFFRANCHISSEMENT']["LIMITE_POIDS"])
            type_env = configplis[section]['AFFRANCHISSEMENT']["ENV_STD"]

            # Si on dépasse le poids de tranche postale, on met à jour les infos dans les index
            # print(poidsenveloppe)
            if poidsenveloppe > poidsmax:
                type_env = configplis[section]['AFFRANCHISSEMENT']["ENV_TRANCHE_SUP"]
                pxaffranchissement = configplis[section]['AFFRANCHISSEMENT']["PRIX_TRANCHE_SUP"]
                self.logwrite.debug(
                    "    Le pli " + str(
                        nom) + " est supérieur à la tranche, on passe à la tarification et enveloppe sup.")
            else:
                pxaffranchissement = configplis[section]['AFFRANCHISSEMENT']["PRIX_STD"]

            pxenveloppe = float(configplis[section][type_env]["PUHT"])
            pdsenveloppe = float(configplis[section][type_env]["POIDS"])

            detail_tarif_postal['pxaffranchissement'] = pxaffranchissement
            detail_tarif_postal['TypeEnveloppe'] = type_env
            detail_tarif_postal['pxenv'] = pxenveloppe
            detail_tarif_postal['pdenv'] = pdsenveloppe
        except (Exception,):
            pass
        return detail_tarif_postal

    @staticmethod
    def get_detail_consommable(ref_consommable, section):
        detail_conso = dict()
        if ref_consommable != "-":
            if ref_consommable not in configplis[section]:
                detail_conso['POIDS'] = float(configplis[section]["PV"]["POIDS"])
                detail_conso['PRIX'] = float(configplis[section]["PV"]["PUHT"])
            else:
                detail_conso['POIDS'] = float(configplis[section][ref_consommable]["POIDS"])
                detail_conso['PRIX'] = float(configplis[section][ref_consommable]["PUHT"])
        else:
            detail_conso['POIDS'] = 0.00
            detail_conso['PRIX'] = 0.00
        return detail_conso


if __name__ == "__main__":
    appname = "TEST_CALCUL_PLIS"

    log = logfile.Logfile(appname)
    _logmode = "info"

    logwrite = log.create_logger(_logmode)

    cp = CalculPlis(logwrite)
    cp.parcours_dir(config["usinecourrier"]["stockdir"])
