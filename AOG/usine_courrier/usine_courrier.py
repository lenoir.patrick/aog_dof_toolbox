#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import mysql.connector
import os
import re
from mysql.connector.errors import OperationalError

# TODO: Factoriser le chargement des json
with open(os.path.join(os.path.dirname(__file__), 'config.json')) as json_data:
    config = json.load(json_data)

with open(os.path.join(os.path.dirname(__file__), "..", 'config.json')) as json_data:
    configgeneral = json.load(json_data)

with open(os.path.join(os.path.dirname(__file__), 'config_pli.json')) as json_data:
    configplis = json.load(json_data)

with open(os.path.join(os.path.dirname(__file__), 'config_produits.json')) as json_data:
    config_produits = json.load(json_data)


def get_pnd(prod_type):
    """
    Retourne la tarification d'un PND en fonction du type de production.

    Args:
        prod_type (str): Le type de production en cours, généralement sous la forme "PND_xxx".

    Returns:
        float: Le coût de traitement d'un PND.

    Raises:
        Exception: Si une clé n'est pas trouvée dans la configuration pour l'entrée "prod_type/PRESTATIONS/PRIX_PND".
            Cela indique une erreur de configuration dans le fichier config_pli.json.
    """
    try:
        # Supprime le préfixe "PND_" pour obtenir la clé correcte dans la configuration
        prod_type = prod_type.replace("PND_", "")

        # Récupère le coût de traitement PND depuis la configuration
        return configplis[prod_type]["PRESTATIONS"]["PRIX_PND"]
    except KeyError:
        msg = f"Erreur de configuration dans le fichier config_pli.json pour l'entrée {prod_type}/PRESTATIONS/PRIX_PND"
        raise Exception(msg)
        msg = "Erreur de configuration dans le fichier config_pli.json pour l'entrée " \
              + prod_type + "/PRESTATIONS/PRIX_PND"
        raise Exception(msg)


def get_type_pnd(motif_pnd):
    """Retourne le type id du PND pour la table de document tracking.

    Args:
        motif_pnd (str): Le motif du PND.

    Returns:
        int: L'id du type PND.
            - 1: Motif non spécifié.
            - 2: Destinataire inconnu à l'adresse.
            - 3: Défaut d’accès ou d'adressage, boîte aux lettres non identifiable, adresse incorrecte.
            - 4: Pli refusé par le destinataire, refus à l'adresse.
            - 5: Délai d'instance dépassé, dépassement de délai d'instance.
    """
    type_pnd = 1

    if "Destinataire inconnu à l'adresse" in motif_pnd:
        type_pnd = 2

    elif any(keyword in motif_pnd for keyword in ["Défaut d’accès ou d'adressage",
                                                  "boîte aux lettres non identifiable",
                                                  "adresse incorrecte"]):
        type_pnd = 3

    elif any(keyword in motif_pnd for keyword in ["Pli refusé par le destinataire",
                                                  "refus à l'adresse"]):
        type_pnd = 4

    elif any(keyword in motif_pnd for keyword in ["délai d'instance dépassé",
                                                  "dépassement de délai d'instance"]):
        type_pnd = 5

    return type_pnd


def get_produit_from_num_contrat(num_contrat):
    """
    Identifie le type de produit à partir d'un numéro de contrat.

    Parameters:
    - num_contrat (str): Le numéro de contrat à analyser.
    - config_produits (list): Une liste de dictionnaires contenant les préfixes de produits et leurs noms associés.

    Returns:
    - str: Le nom du produit si une correspondance est trouvée, sinon retourne "-".

    Example:
    >>> config_produits = [
    ...     {"prefixe": "CPMP", "produit": "MRH_SOLIDAIRE_MDP"},
    ...     {"prefixe": "CPSO", "produit": "MRH_SOLIDAIRE_VYV"},
    ...     # Ajoutez d'autres éléments de configuration au besoin
    ... ]
    >>> num_contrat_test = "CPMP123456"
    >>> get_produit_from_num_contrat(num_contrat_test, config_produits)
    "MRH_SOLIDAIRE_MDP"
    """

    try:
        for item in config_produits:
            pattern = re.compile(r'^' + item["prefixe"] + r'\d+$')
            if pattern.match(num_contrat):
                print(f"{num_contrat} - Le contrat est de type {item['produit']}.")
                return item['produit']
    except Exception as e:
        print(f"Une erreur s'est produite : {e}")
        return "-"

    return "-"


class UsineCourrier:
    def __init__(self, logwrite, file=None):
        self.logwrite = logwrite
        self._conn = mysql.connector.connect(user=config["database"]["user"], password=config["database"]["password"],
                                             host=config["database"]["host"], database=config["database"]["database"],
                                             allow_local_infile=True)
        self._cursor = self._conn.cursor()
        self.__idx_fichier = 1
        self.__nb_sql_lines = 0
        self.file = file
        if self.file is not None:
            self.open_sql_file()
        else:
            self.sqlfileopen = None

    def open_sql_file(self):
        self.__idx_fichier = self.__idx_fichier + 1
        self.sqlfile = os.path.join(configgeneral["usinecourrier"]["stockdir"], self.file + "_" + str(self.__idx_fichier).zfill(3) + ".sql")
        self.sqlfileopen = open(self.sqlfile, "w")

    def set_sql(self, sql):

        result_iterator = self._cursor.execute(sql, multi=True)
        if self.sqlfileopen != None:
            self.sqlfileopen.write(sql + "\n")
            self.sqlfileopen.flush()

    def set_sql_lot(self, sql):
        """
        Ajout d'une ligne SQL dans le fichier ouvert en cours

        Args:
            sql: Requete SQL à ajouter
        """
        if self.sqlfileopen is not None:
            self.sqlfileopen.write(sql + "\n")
            self.sqlfileopen.flush()
        self.__nb_sql_lines = self.__nb_sql_lines + 1
        if self.__nb_sql_lines > 150:
            self.open_sql_file()
            self.__nb_sql_lines = 0

    def upload_sql_file(self, sqlfile):
        """
        Chargment dans la base usine courrier d'un fichier SQL

        Args:
            sqlfile: le chemin d'accès au fichier

        Raises:
            Exception: Timeout sur la connexion à la base
        """
        try:
            idx = 1
            with open(sqlfile) as sql:
                result_iterator = self._cursor.execute(sql.read(), multi=True)
                # TODO: Affiche uniquement en mode debug
                for res in result_iterator:
                    # print("Running query: ", res)  # Will print out a short representation of the query
                    self.logwrite.info(str(idx).zfill(3) + " - " + f"Affected {res.rowcount} rows")
                    idx = idx + 1
            self.commit()
            self.logwrite.info("Traitement terminé avec succès. Base mise à jour")
        except OperationalError:
            msgerr = "Délai de réponse de la base dépassé. L'import n'a pu avoir lieu. Réessayer dans un moment."
            self.logwrite.error(msgerr)
            raise Exception(msgerr)
        except Exception:
            msgerr = "Erreur lors de l'import en base. Le process s'est anormalement terminé. Base non mise à jour."
            self.logwrite.error(msgerr)
            raise Exception(msgerr)

    def select_sql(self, sql):
        self._cursor.execute(sql)
        result = self._cursor.fetchall()
        return result

    def set_csv_file(self, csvfile):
        self._cursor.execute(
            "LOAD DATA LOCAL INFILE '" + csvfile + "' INTO TABLE usine_courrier FIELDS TERMINATED BY ','  LINES TERMINATED BY '\r\n'")

    def set_pnd(self, numcontrat, appname):
        prixpnd = get_pnd(appname)

        prodname = appname.replace("PND_", "")

        if "COGEPRINT_TERMES" in prodname:

            sql1 = "UPDATE `usine_courriers_editique`.`usine_courrier` set pnd = true, pxpnd = " + str(prixpnd) + " WHERE ReferenceContrat LIKE '%" + numcontrat + "%' AND DateTraitement >= NOW() - INTERVAL 6 MONTH AND production_papier_ID LIKE '%" + prodname + "%';\n"
            self.set_sql(sql1)

            sql = "SELECT production_papier_ID from `usine_courriers_editique`.`usine_courrier` WHERE ReferenceContrat LIKE '" + numcontrat + "%' AND DateTraitement >= NOW() - INTERVAL 6 MONTH AND production_papier_ID LIKE '%" + prodname + "%'"
            result = self.select_sql(sql)
            if len(result) != 0:
                production_papier_id = result[0][0]
                sql = "UPDATE `usine_courriers_editique`.`production_papier` SET nbpnd = nbpnd + 1, cout_pnd = cout_pnd + " + str(prixpnd) + " WHERE ID = '" + production_papier_id + "'"
                self.set_sql(sql)
                self.commit()
        else:
            sql = "UPDATE usine_courriers_editique.usine_courrier SET pnd = true, pxpnd = " + str(prixpnd) + " WHERE uuidpli = '" + numcontrat + "';"
            self.set_sql(sql)
            sql = "SELECT production_papier_ID from `usine_courriers_editique`.`usine_courrier` WHERE  uuidpli = '" + numcontrat + "';"
            result = self.select_sql(sql)
            if len(result) != 0:
                production_papier_id = result[0][0]
                sql = "UPDATE `usine_courriers_editique`.`production_papier` SET nbpnd = nbpnd + 1, cout_pnd = cout_pnd + " + str(prixpnd) + " WHERE ID = '" + production_papier_id + "'"
                self.set_sql(sql)

    def commit(self):
        """
        Commit des requetes en attente de validation.
        Fin de la transaction
        """
        self._conn.commit()
