# pip install mysql-connector-python
# import json

from os import listdir
from os.path import isfile, join

prod = "D:\\temp\\stock_usine_courrier\\1 - PROD\\"
dirout = "D:\\temp\\stock_usine_courrier\\"

TAB_1 = "-\t-\t-\t1\t-\t-\t"
TAB_2 = "-\t-\t2\t1\t-\t-\t"

# Script de transformation des fichiers de pli de prod avant envoie en BDD
# A ne faire qu'une fois, car les nommages ont été modifiés et déployés.

for file in listdir(prod):
    filein = join(prod, file)
    if isfile(filein) and file.endswith(".ind") and "_pnd_" not in file:
        filerename = file.replace("pli_cogeprint_lr", "PLI_COGEPRINT_LR_STD")
        filerename = filerename.replace("pli_termessogessur", "PLI_PREVOIR_TERMES_SOGESSUR")

        filerename = filerename.replace("_pli_", "_PLI_")

        filerename = filerename.replace("impressionprevoir", "PREVOIR_GESTION_STD")
        filerename = filerename.replace("lc_cogeprint", "COGEPRINT_LC_STD")
        filerename = filerename.replace("PLI_TERMES_AIG", "PLI_COGEPRINT_TERMES_AIG")
        filerename = filerename.replace("PLI_TERMES_STD", "PLI_COGEPRINT_TERMES_STD")
        fileout = join(dirout, filerename)

        print(file + " > " + filerename)

        openfile = open(filein, "r")
        outfile = open(fileout, "w")
        firstline = True
        changefirstcol = False
        addCPcol = False
        for line in openfile:
            if firstline is True:
                # Vérification du type d'index (ancienne version ou non)
                if line.startswith("_@#_vpf_idx"):
                    changefirstcol = True
                if "\tCP\t" not in line:
                    addCPcol = True
                firstline = False

            if changefirstcol is True:
                delete = line.split("\t")[0]
                line2 = line.replace(delete + "\t", "")
            else:
                line2 = line

            lastcar = (line2[-2:])
            if lastcar == "\t\n":
                line2 = line2.replace(lastcar, "\n")

            line2 = line2.replace("idLot\tcanalDistribution", "idLot\tCP\tcanalDistribution")
            if addCPcol is True:
                line2 = line2.replace("\tIMPRESSION\t", "\t-\tIMPRESSION\t")
                line2 = line2.replace("\tEMAIL\t", "\t-\tEMAIL\t")

            line2 = line2.replace("PRV_ENV", "ENV")
            line2 = line2.replace("ENV_", "ENV-")
            line2 = line2.replace("PRV_PAP", "PAP")
            line2 = line2.replace("PAP_", "PAP-")
            line2 = line2.replace("PRV_PV", "PV")
            line2 = line2.replace("PRV_CONSTAT", "CONSTAT")
            line2 = line2.replace("PAPIER_BLANC", "PAPIER-BLANC")

            DateTraitement = line2.split("\t")[2]
            DateImpression = line2.split("\t")[3]
            if len(DateTraitement) == 8:
                # print("datet")
                DateTraitement = DateTraitement[0:4] + "-" + DateTraitement[4:6] + "-" + DateTraitement[6:8]
                line2 = line2.replace(line2.split("\t")[2] + "\t", DateTraitement + "\t")

            if len(DateImpression) == 8:
                DateImpression = DateImpression[0:4] + "-" + DateImpression[4:6] + "-" + DateImpression[6:8]

            if "COGEPRINT_TERMES" in filerename:
                line2 = line2.replace("ENV-C5", "ENV-C6")

                if "CVDEFINITIVE" in line2:
                    # print(line2.split("\t")[20])
                    line2 = line2.replace(TAB_1, "-\t-\t1\t1\t-\t-\t")
                elif "AVISECHEAN" in line2:
                    if "_2ROUES_" in line:
                        line2 = line2.replace(TAB_1, TAB_2)
                        line2 = line2.replace(TAB_2, "-\t-\t2\t1\tPV2R\t-\t")
                    else:
                        line2 = line2.replace(TAB_1, TAB_2)
                else:
                    line2 = line2.replace("R\t-\t-\t-\t-", "RV\t-\tPAPIER-BLANC\tENV-C6\t2")

            elif "COGEPRINT_LC" in filerename:
                uuid = "COGEPRINT_LC_" + line2.split("\t")[4]
                line2 = line2.replace(line2.strip().split("\t")[-1], uuid)

            elif "PREVOIR_GESTION" in filerename:
                for item in ["CVFACS", "ATTPROV", "SIN_DEC_CIRC_ROULANT", "MANDAT_SEPA_COMMUN",
                             "SANT_PRVR_TAB_GAR_COMMUN", "SANT_EQ_TAB_GAR_COMMUN", "RI-FLOTTE"]:
                    if item in line2:
                        line2 = line2.replace("IMPRESSION\tPREVOIR", "EMAIL\tASSURONE")
                if "PRV-REMA" in line2:
                    line2 = line2.replace("PRV-REMA", "PRV-NEUTRE")
                if "PRV-SOPE" in line2:
                    line2 = line2.replace("PRV-SOPE", "PRV-PEUG")

            outfile.write(line2)
