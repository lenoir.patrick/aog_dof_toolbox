#!python3
# -*- coding: utf-8 -*-
import os
import json
from AOG.tools import GeneralConfig, get_time_stamp_aaaammjj
from datetime import datetime
import AOG.constants as constants


class SupervisionTraitements(GeneralConfig):
    def __init__(self, appname):
        """Constructeur

        Args:
            appname: Nom de l'application en cours
        """
        self.appname = "SUPERVISION_" + appname
        super().__init__(self.appname)
        self.logwrite.info(constants.ET_SUPERVISION + "Instanciation de la classe : " + self.__class__.__name__)

        self.jsonfinal = {}
        self.listapplis = ["PREVOIR_TERMES_STD", "PREVOIR_GESTION_STD", "REASSORT_CONSOMMABLES",
                           "COGEPRINT_TERMES_AIG", "COGEPRINT_TERMES_SANTE", "COGEPRINT_TERMES_ATTSCO",
                           "COGEPRINT_TERMES_STD", "PND_COGEPRINT_TERMES",
                           "COGEPRINT_LR_STD", "TRAITEMENT_LR_BDF_COGEPRINT", "TRAITEMENT_LISTES_LR",
                           "PND_COGEPRINT_LR",
                           "COGEPRINT_LC_STD", "PND_COGEPRINT_LC"]

        self.date_jour = get_time_stamp_aaaammjj()

        self.itemtmp = ""
        self.listeitem = []
        self.log_dir = self.get_log_dir()
        self.logwrite.info(constants.ET_SUPERVISION + "Recherche dans le répertoire (logdir) : " + self.log_dir)

    def __load_apps(self):
        """
        Chargement des paramètres d'application
        """
        # Récupération des derniers fichiers de chaque traitement
        # self.logwrite.info(constants.ET_SUPERVISION + "Liste des apps à rechercher : " + str(self.listapplis))
        self.logwrite.info(constants.ET_SUPERVISION + "Préparation des informations")
        for item in self.listapplis:
            app = {'result': True, 'warning': False}
            self.jsonfinal[item] = app

    def __write_json_output(self):
        """
        Ecriture du fichier json de sortie qui sera ensuite lu par l'interface de supervition éditique
        """
        output_dir = os.path.join(self.get_tmp_dir(), "django_interface")
        os.makedirs(output_dir, exist_ok=True)
        output_file = os.path.join(output_dir, "logs.json")
        with open(output_file, "w") as outfile:
            outfile.write(json.dumps(self.jsonfinal, indent=4))

    def __del__(self):
        """Constructeur de suppression
        """
        pass

    def __get_logs(self):
        """Méthode

        Args:


        Returns:
            Retour

        Raises:
        """
        for item in reversed(os.listdir(self.log_dir)):
            # print(item)
            item_lu = item[0:-20]
            if item_lu in self.listapplis and item_lu != self.itemtmp:
                self.itemtmp = item_lu
                self.logwrite.debug(
                    constants.ET_SUPERVISION + "Log trouvé pour l'application : " + self.itemtmp + " / " + item)

                self.listeitem.append(item)
                fichier_log = os.path.join(self.log_dir, item)
                if fichier_log.endswith(".log") and "SUPERVISION" not in fichier_log:

                    # Calcul de la date
                    date_traitement = item[-19:-11]
                    date_traitement = date_traitement[0:4] + "-" + date_traitement[4:6] + "-" + date_traitement[6:8]

                    d1 = datetime.strptime(self.date_jour, "%Y-%m-%d")
                    d2 = datetime.strptime(date_traitement, "%Y-%m-%d")
                    diff_date = abs((d2 - d1).days)

                    with open(fichier_log, 'r') as open_log:
                        lst_errors = []
                        lst_warnings = []
                        lst_ok = []
                        result = "ok"
                        try:
                            for line in open_log:
                                line_ecrite = line[24:200].strip()
                                if "[ERROR" in line:
                                    # self.logwrite.error("     " + line.strip())
                                    self.jsonfinal[item_lu]['result'] = False
                                    lst_errors.append(line_ecrite)
                                    result = "error"
                                elif "[WARNING" in line:
                                    # self.logwrite.error("     " + line.strip())
                                    self.jsonfinal[item_lu]['warning'] = True
                                    lst_warnings.append(line_ecrite)
                                    if result != "error":
                                        result = "warn"
                                elif "[INFO    ] SVISION" in line:
                                    # print(line)
                                    lst_ok.append(line_ecrite.replace("SVISION", ""))

                            self.jsonfinal[item_lu]['lst_errors'] = lst_errors
                            self.jsonfinal[item_lu]['lst_warnings'] = lst_warnings
                            self.jsonfinal[item_lu]['lst_ok'] = lst_ok
                            heure_affiche = item[-10:-4]
                            date_affiche = "{0} {1}:{2}:{3}".format(date_traitement, heure_affiche[0:2],
                                                                    heure_affiche[2:4], heure_affiche[4:6])
                            self.jsonfinal[item_lu]['date_traitement'] = date_affiche
                            self.jsonfinal[item_lu]['date_diff'] = diff_date
                            self.jsonfinal[item_lu]['result'] = result
                        except Exception as ex:
                            print("########" + str(ex))
        self.__write_json_output()
        self.logwrite.info(constants.ET_SUPERVISION + "Fin du traitement supervision et mise à jour du json")

    def run(self):
        """
        Lancement du traitement
        """
        self.__load_apps()
        self.__get_logs()


if __name__ == "__main__":
    appname = os.path.basename(__file__.upper())
    st = SupervisionTraitements(appname)
    st.run()
