#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

from office365.runtime.auth.user_credential import UserCredential
from office365.sharepoint.client_context import ClientContext
from office365.sharepoint.files.file import File
import os
import json
import AOG.logfile as logfile
import AOG.constants as constants


class SharepointException(ValueError):
    def __init__(self, logwrite, ex, m="Erreur de traitement Sharepoint"):
        self.message = m
        self.ex = ex
        self.logwrite = logwrite
        self._entete = constants.ET_SHAREPOINT

        self.logwrite.error(self._entete + self.message)

    def __str__(self):
        return "{}: {}".format(type(self.ex).__name__, self.ex)


class Sharepoint:
    def __init__(self, logwrite):
        """Instanciation de la classe Sharepoint

        Args:
            logwrite: Instance de log
        """
        if logwrite is None:
            raise logfile.LogFileException

        with open(os.path.join(os.path.dirname(__file__), 'config.json')) as json_data:
            self.config = json.load(json_data)

        os.environ["HTTP_PROXY"] = self.config["sharepoint"]["proxy"]

        login = self.config["sharepoint"]["login"]
        password = self.config["sharepoint"]["pass"]
        sp_site = self.config["sharepoint"]["sp_site"]
        self.tmpdir = self.config["sharepoint"]["tmpdir"]

        self.logwrite = logwrite
        self.delete_input = True
        self._entete = constants.ET_SHAREPOINT
        try:
            client_credentials = UserCredential(login, password)
            self.ctx = ClientContext(sp_site).with_credentials(client_credentials)
        except Exception as ex:
            raise SharepointException(logwrite, ex, "Erreur lors de la connexion à SharePoint")

        self.logwrite.info(self._entete + "Connexion au serveur ok")
        self.localfile = None

    def upload(self, remote_file, local_file=None, delete_input=True):
        """Charge le fichier sur Sharepoint

        Args:
            remote_file: Chemin vers le fichier à charger
            local_file: Fichier local à charger
            delete_input: Supprimer le fichier local
        """
        if local_file is None:
            local_file = self.localfile
        else:
            self.localfile = local_file

        self.delete_input = delete_input

        try:
            with open(local_file, 'rb') as content_file:
                file_content = content_file.read()
                self.logwrite.info(self._entete + "Lecture du fichier " + local_file)

            dir_name, name = os.path.split(remote_file)
            self.ctx.web.get_folder_by_server_relative_url(dir_name).upload_file(name, file_content).execute_query()
            self.logwrite.info(self._entete + "Téléchargement du fichier vers " + remote_file)

            self._del_tmp_file()
        except Exception as ex:
            raise SharepointException(self.logwrite, ex, "Chargement du fichier impossible.")

    def download(self, remote_file) -> str:
        """Télécharger un fichier de Sharepoint

        Args:
            remote_file: Chemin du fichier à récupérer

        Returns:
            Fichier local
        """
        try:
            # Get remote file
            file_response = File.open_binary(self.ctx, remote_file)
            self.logwrite.info(self._entete + "Lecture du fichier vers " + remote_file)

            # Saving file to local
            local_file = os.path.join(self.tmpdir, os.path.basename(remote_file))
            with open(local_file, 'wb') as output_file:
                output_file.write(file_response.content)

                self.logwrite.info(self._entete + "Création du fichier " + local_file)

            self.localfile = local_file
            return local_file
        except ValueError as ex:
            raise SharepointException(self.logwrite, ex, "Récupération du fichier impossible")

    def _del_tmp_file(self):
        if self.delete_input is True and os.path.isfile(self.localfile):
            self.logwrite.info(self._entete + "Suppression du fichier : " + self.localfile)
            os.remove(self.localfile)
