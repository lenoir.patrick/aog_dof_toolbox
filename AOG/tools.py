#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
import json
import shutil
import time
import os
from datetime import datetime, timedelta
from docx import Document

import AOG.logfile as logfile
from AOG.sendmailtools import Sendmail

with open(os.path.join(os.path.dirname(__file__), 'config.json')) as json_data:
    config = json.load(json_data)


class NoSpaceLeft(Exception):
    def __init__(self, logwrite):
        """
        Initialise une exception NoSpaceLeft pour signaler un espace disque insuffisant.

        Args:
        logwrite: Fichier de log à utiliser pour enregistrer l'erreur.

        Raises:
        NoSpaceLeft: Cette exception est levée pour indiquer un problème d'espace disque insuffisant.

        Example:
        try:
            # Votre code qui peut provoquer une exception NoSpaceLeft
        except NoSpaceLeft as e:
            log_file = e.logwrite
            log_file.error("L'espace disque nécessaire est insuffisant. Le traitement est stoppé")
        """
        logwrite.error("L'espace disque nécessaire est insuffisant. Le traitement est stoppé")


class GeneralError(Exception):
    def __init__(self, error_msg):
        """Classe d'erreur générale

        Args:
            error_msg: Message d'erreur
        """
        self.error_msg = error_msg
        self.__appname = ""
        self.__logfilepath = ""

    def __str__(self):
        """Affichage de l'exception
        """
        return "[ERROR ]: {}".format(self.error_msg)

    def set_params(self, appname, logfilepath):
        """
        Définit les paramètres de l'objet en attribuant les valeurs spécifiées.

        Args:
        appname (str): Le nom de l'application à associer à l'objet.
        logfilepath (str): Le chemin du fichier de journalisation à associer à l'objet.

        Returns:
        None

        Note:
        Cette méthode permet de configurer les paramètres d'un objet en attribuant les valeurs fournies. Elle affecte les attributs privés '__appname' et '__logfilepath' de l'objet aux valeurs spécifiées.

        Exemple d'utilisation :
        obj = MaClasse()
        obj.set_params("MonApp", "/chemin/vers/mon/fichier.log")
        print(obj.__appname)  # Affichera "MonApp"
        print(obj.__logfilepath)  # Affichera "/chemin/vers/mon/fichier.log"
        """
        self.__appname = appname
        self.__logfilepath = logfilepath

    def send_error_mail(self):
        """Envoie un message d'erreur sur l'ano rencontrée

        """
        sm = Sendmail()
        sm.set_subject("Erreur générale sur l'application " + self.__appname)
        sm.set_high_priority()
        sm.add_attachment(self.__logfilepath)
        body = "Une erreur générale est survenir dans lors de l'execution de l'application " + self.__appname + "\n\n"
        body = body + "Erreur : \n"
        body = body + "    " + self.error_msg + "\n\n"
        body = body + "Le traitement n'a pas été à son terme.\n\n"
        body = body + "Le fichier de log est joint à l'application. Merci de le lire pour analyse et d'informer les"
        body = body + " services nécessaires le cas échéant"
        sm.set_body(body)

        sm.go_mail()


class GeneralConfig:
    def __init__(self, appname):
        """Classe de configuration générale d'AOG

        Args:
            appname: Nom du traitement en cours
        """
        self.__config = config
        self.__debug = self.__config["debug"]
        if self.__debug is False:
            self.__log_info = self.__config["logInfo"]
        else:
            self.__log_info = "debug"

        self.env = self.__config['env']

        self.log = logfile.Logfile(name=appname)
        __logmode = self.__log_info

        if __logmode not in ["info", "warn", "debug", "error"]:
            __logmode = "info"

        debugparams = {'env': self.env, 'debug': self.__debug, 'logmode': __logmode}
        self.logwrite = self.log.create_logger(__logmode, True, debugparams)

    def is_env_prod(self) -> bool:
        """Méthode qui détermine si on se trouve sur un environnement de procution

        Returns:
            Traitement sur un env de production
        """
        if self.env == "PROD":
            return True
        else:
            return False

    def is_debug_mode(self) -> bool:
        """Méthode qui détermine si on est en mode __debug

        Returns:
            Mode __debug activé
        """
        return self.__debug

    def get_tmp_dir(self) -> str:
        """Retourne le répertoire temporaire par défaut

        Returns:
            Le répertoire par défaut (config/dir/tmpdir)
        """
        return str(self.__config['dirs']['tmpdir'])

    def get_log_dir(self) -> str:
        """Retourne le répertoire de log par défaut

        Returns:
            Le répertoire par défaut (config/dir/tmpdir)
        """
        return str(self.__config['dirs']['logdir'])


def get_time_stamp() -> str:
    """Retourne une date au format AAAAMMJJHHMMSS

    Returns:
        La date du jour
    """
    ts = time.gmtime()
    return time.strftime("%Y%m%d%H%M%S", ts)


def get_time_stamp_aaaammjj() -> str:
    """Retourne une date au format AAAAMMJJ

    Returns:
        La date du jour
    """
    ts = time.gmtime()
    return time.strftime("%Y-%m-%d", ts)


def get_working_directory(appname) -> str:
    """Créé un environnement de travail unique

    Args:
        appname: Nom de l'application utilisée

    Returns:
        Le chemin d'accès généré
    """
    rootworkingdir = config["dirs"]["workingdirectory"]
    try:
        os.makedirs(rootworkingdir, exist_ok=True)
    except (Exception, ):
        rootworkingdir = os.getcwd()

    # TO DO Purger les répertoires existants

    rep_travail = os.path.join(rootworkingdir, appname + "_" + datetime.now().strftime('%Y%m%d%H%M%S'))
    if os.path.isdir(rep_travail) is True:
        shutil.rmtree(rep_travail)
    os.makedirs(rep_travail)
    return rep_travail


def disk_space(disque, logwrite=None):
    """Affiche le taux d'espace disponible sur le disque

    Args:
        disque: Répertoire à tester
        logwrite: Pointeur vers le fichier de log
    """
    if logwrite is None:
        raise logfile.LogFileException("Aucun fichier de log défini")

    disk_memory = shutil.disk_usage(disque)
    pourcentage_libre = int((disk_memory[2] * 100) / disk_memory[0])
    pourcentage_necessaire = int(config["pourcentage_minimum"])

    logwrite.info("Espace disque requis : " + str(pourcentage_necessaire) + "Go vs espace nécessaire : " +
                  str(pourcentage_libre) + "Go")

    if pourcentage_libre > pourcentage_necessaire:
        logwrite.info("Espace disque suffisant, le traitement continue")
    else:
        raise NoSpaceLeft(logwrite)


def replace_docx(file, old, new):
    """Remplace une occurence texte par une autre

    Args:
        file: Fichier à modifier
        old: Occurence à remplacer
        new: Nouvelle occurence texte
    """
    document = Document(file)
    for paragraph in document.paragraphs:
        new_word = paragraph.text.replace(old, new)
        paragraph.text = new_word
        document.save(file)


def calc_jour_ouvre(jour, typetraitement, sstype, paramtab=None) -> tuple[int, bool]:
    """Calcule le prochain jour ouvré à partir du jour actuel

    Args:
        jour: Jour à tester
        typetraitement: Type de traitement en cours pour du spécifique
        sstype: Sous type de traitement
        paramtab: Chemin d'accès vers une table de jours férié

    Returns:
        tuple: Un tuple contenant la nouvelle date d'envoi (str) et un booléen indiquant si le coût de traitement doit être ajouté.

    Raises:
        Exception: En cas d'erreur non spécifiée.
    """
    # TODO: Ajout la gestion de jours fériés/non travaillés
    try:
        add_cout_traitement = True

        dateimpression = jour  # + datetime.timedelta(days=1)
        dateimpression = datetime(int(dateimpression[0:4]), int(dateimpression[4:6]), int(dateimpression[6:8]), 0, 0)

        # Est-un jour de WE (pour les LR)
        # Si le jour est un WE, on force à tomber le lundi
        if dateimpression.weekday() == 5:  # Samedi alors on saute le WE
            dateimpression, add_cout_traitement = calc_jour_ouvre(str(dateimpression + timedelta(days=2)).replace("-", "")[0:8],
                                                  typetraitement, sstype)
            dateimpression = datetime(int(dateimpression[0:4]), int(dateimpression[4:6]),
                                      int(dateimpression[6:8]), 0, 0)
            add_cout_traitement = False
        elif dateimpression.weekday() == 6:  # Dimanche alors on saute le WE
            dateimpression, add_cout_traitement = calc_jour_ouvre(str(dateimpression + timedelta(days=1)).replace("-", "")[0:8],
                                                  typetraitement, sstype)
            dateimpression = datetime(int(dateimpression[0:4]), int(dateimpression[4:6]),
                                      int(dateimpression[6:8]), 0, 0)
            add_cout_traitement = False

        # Ajustement de la date d'impression estimée
        if typetraitement == "LR":
            dateimpression = dateimpression + timedelta(days=1)
        elif typetraitement == "TERMES" and sstype != "SOGESSUR":
            dateimpression = dateimpression + timedelta(days=6)

        # Ajustement des jours fériés/chômés
        try:
            tabjour_ouvres = open(os.path.join(paramtab, "tg_jours_ouvres.tab"), 'r')
            jourouvre = None
            for line in tabjour_ouvres:
                line = line.strip()
                try:
                    datetest = datetime(datetime.date.today().year, int(line.split("-")[1]),
                                        int(line.split("-")[0]), 0, 0)
                    if datetest == jourouvre:
                        jourouvre = calc_jour_ouvre(jourouvre + timedelta(days=1),
                                                         typetraitement, sstype, paramtab)
                except (Exception, ):
                    pass
            tabjour_ouvres.close()
        except (Exception, ):
            pass

        # Reformulation de la date en String
        dateimpression = str(dateimpression).replace("-", "")[0:8]

        return dateimpression, add_cout_traitement
    except Exception as ex:
        print(str(ex))
        return -1, False


def nom_fichier_sans_extension(fichier) -> tuple[str, str]:
    """ Retourne le nom de fichier et son extension

    Args:
        fichier: nom du fichier (sans son chemin)

    Returns:
          Le nom du fichier, son extension
    """
    basename = os.path.basename(fichier)
    file_name = os.path.splitext(basename)[0]
    ext = os.path.splitext(basename)[1]
    return file_name, ext


def maj_supervision(appname):
    from AOG.supervision_traitements import SupervisionTraitements
    st = SupervisionTraitements(appname)
    st.run()


if __name__ == "__main__":

    appname = os.path.basename(__file__.upper())
    gc = GeneralConfig(appname)

    gc.logwrite.info(get_time_stamp())
    gc.logwrite.info(get_time_stamp_aaaammjj())
    # get_working_directory
    disk_space("D:", gc.logwrite)
    # replace_docx
    # calc_jour_ouvre
