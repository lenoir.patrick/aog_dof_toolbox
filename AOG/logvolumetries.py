#!python3
# -*- coding: utf-8 -*-
import os
from openpyxl import load_workbook
import datetime
import AOG.sharepoint as sharepoint
from AOG.sharepoint import SharepointException
import AOG.constants as constants
import AOG.logfile as logfile
from AOG.sendmailtools import ErrorSendMail


class Volumetries:
    def __init__(self, onglet, logwrite=None, year=None, env=False, generic=False):
        """
        Constructeur de la classe pour logguer les volumétries exploitées dans les traitements éditiques.

        Args:
            onglet: Nom de l'onglet à utiliser
            logwrite: Pointeur vers le fichier de log
            year: Année de traitement
            env: Environnement de travail
            generic: Choix du fichier sur lequel travailler

        Raises:
            Exception: Aucun logwrite n'est utilisé
        """
        # Le flag generic est utilisé pour choisir le fichier de volumétrie annuel en écriture, ou bien le fichier
        # _Editique.xlsx_ en lecture pour vérification d'informations

        if logwrite is None:
            raise logfile.LogFileException

        now = datetime.datetime.today()
        if year is None:
            year = now.year

        self.logwrite = logwrite
        rootfile = "/sites/DSI/Documents partages/Editique/VOLUMETRIES/"
        ext_xlsx = constants.XLSX_EXT
        if generic is False:
            if env == "PROD":
                self.xlsx_sp_file = os.path.join(rootfile, "VOLUMETRIES_" + str(year) + ext_xlsx)
            elif env == "DEV":
                self.xlsx_sp_file = os.path.join(rootfile, "VOLUMETRIES_DEV_" + str(year) + ext_xlsx)
            elif env == "PPROD":
                self.xlsx_sp_file = os.path.join(rootfile, "VOLUMETRIES_PPROD_" + str(year) + ext_xlsx)
            else:
                self.xlsx_sp_file = os.path.join(rootfile, "VOLUMETRIES_TESTS_" + str(year) + ext_xlsx)
        else:
            self.xlsx_sp_file = os.path.join(rootfile, "_EDITIQUE_" + ext_xlsx)

        # Récupération du fichier sharepoint

        self.tab = onglet
        try:
            self.sp = sharepoint.Sharepoint(self.logwrite)
            self.openxlsxfile()
        except SharepointException:
            raise ErrorSendMail(gc, appname)

        self.nbvaluesadded = 0

        self.logwrite.info(constants.ET_VOLUMETRIES + "Initialisation ok")

        self.coldeb = 0
        self.rowdeb = 0
        self.yeardeb = 0

    def openxlsxfile(self):
        """ Ouverture du fichier xlxs temporaire"""
        try:
            self.xlsxfile = self.sp.download(self.xlsx_sp_file)
            self.wb = load_workbook(self.xlsxfile, data_only=True)
            self.logwrite.info(constants.ET_VOLUMETRIES + "Ouverture du fichier " + self.xlsxfile)
        except Exception as ex:
            msg = constants.ET_VOLUMETRIES + "Le fichier " + self.xlsxfile + constants.PAS_ACCESSIBLE
            self.logwrite.error(msg)
            # self.logwrite.error(constants.ET_VOLUMETRIES + str(ex))
            raise SharepointException(self.logwrite, msg)

        try:
            self.logwrite.info(constants.ET_VOLUMETRIES + "Chargement de l'onglet " + self.tab)
            self.ws = self.wb[self.tab]
        except Exception as ex:
            msg = constants.ET_VOLUMETRIES + "L'onglet " + self.tab + constants.PAS_ACCESSIBLE
            self.logwrite.error(msg)
            # self.logwrite.error(constants.ET_VOLUMETRIES + str(ex))
            raise SharepointException(self.logwrite, msg)

    def startarray(self, col, row, year):
        """Positionnement dans la bonne cellule du fichier

        Args:
            col: Numéro de colonne
            row: Numéro de ligne
            year: Année utilisée pour un décalage
        """
        self.coldeb = col
        self.rowdeb = row
        self.yeardeb = year
        self.logwrite.debug(constants.ET_VOLUMETRIES + "La case de départ est col: " + str(col) + " / row " + str(row))
        self.logwrite.debug(constants.ET_VOLUMETRIES + "L'année de départ est : " + str(year))

    def setvalue(self, year, month, day, nbenvoisposte):
        """Ecriture dans la cellule déterminée

        Args:
            year: Année en cours
            month: Mois selectionné
            day: Jour en cours
            nbenvoisposte: Valeur à insérer dans la cellule
        """
        rowval = month + self.rowdeb + ((year - self.yeardeb) * 12) - 1
        colval = day + self.coldeb - 1
        self.ws.cell(row=rowval, column=colval).value = int(nbenvoisposte)
        self.nbvaluesadded = self.nbvaluesadded + 1
        if self.nbvaluesadded == 15:
            self._savearray()
            self.nbvaluesadded = 0
        self.logwrite.debug(constants.ET_VOLUMETRIES + "dans la colonne " + str(colval) + " / ligne " + str(rowval))

        self.logwrite.info(constants.ET_VOLUMETRIES + "Volumétrie de " + str(nbenvoisposte) + " plis en date du " +
                           str(day).zfill(2) + " " + str(constants.MONTHS[month - 1]) + " " + str(year))

    def _savearray(self):
        """Enregistrement du fichier temporaire et chargement sur SharePoint"""
        try:
            if os.path.isfile(self.xlsxfile):
                self.wb.save(self.xlsxfile)
                self.logwrite.info(constants.ET_VOLUMETRIES + "Enregistrement du fichier " + self.xlsxfile)
                self.sp.upload(self.xlsx_sp_file)
        except Exception as ex:
            self.logwrite.error(constants.ET_VOLUMETRIES + "Enregistrement du fichier " + self.xlsxfile + " impossible")
            self.logwrite.error(constants.ET_VOLUMETRIES + str(ex))
            raise

    def closefile(self):
        """Fin du traitement et fermeture du fichier"""
        self._savearray()
        self.logwrite.info(constants.ET_VOLUMETRIES + "Fin de la transaction")


if __name__ == "__main__":
    import AOG.tools as aogtools
    appname = os.path.basename(__file__.upper())
    gc = aogtools.GeneralConfig(appname)

    lv = Volumetries("LR COGEPRINT", gc.logwrite, env=gc.env)
    lv.closefile()
