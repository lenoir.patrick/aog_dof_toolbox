#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from smtplib import SMTP, SMTPRecipientsRefused
from email.encoders import encode_base64
import os
import json
import sys


class ErrorSendMail:
    def __init__(self, gc, appname):
        """Constructeur de l'erreur d'envoi de mail

        Args:
            gc: Paramétrage de l'environnement
            appname: Nom de l'application en cours
        """
        errormail = Sendmail(gc.logwrite)

        errormail.set_subject("[FATAL ERROR] " + appname)
        body = "Une erreur fatale s'est produite lors du traitement " + appname + "\n\n"
        body = body + "Voir la log ci-jointe."
        errormail.set_body(body)
        errormail.set_high_priority()
        errormail.add_attachment(gc.log.get_log_path())
        errormail.go_mail()
        sys.exit(-1)


class Sendmail:
    def __init__(self, logwrite=None):
        """Constructeur de la classe Sendmail AssurOne Editique

        Args:
            logwrite: Pointeur vers le fichie de log
        """
        with open(os.path.join(os.path.dirname(__file__), 'config.json')) as json_data:
            self.config = json.load(json_data)
        self.email = MIMEMultipart()
        self.email['From'] = self.config["mail"]["from"]
        self.email['To'] = self.config["mail"]["to"]
        self.address = self.config["mail"]["host"]
        self.port = int(self.config["mail"]["port"])
        self.env = self.config["env"]

        self.body = None
        self.subject = None

        self.logwrite = logwrite

    def add_attachment(self, path):
        """Ajoute un fichier d'accompagnement au mail

        Args:
            path: Chemin d'accès au fichier
        """
        attachment = MIMEBase('application', "octet-stream")
        attachment.set_payload(open(path, "rb").read())
        encode_base64(attachment)
        attachment.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(path))
        self.email.attach(attachment)

    def set_low_priority(self):
        """Définit une priorité basse au mail"""
        self.email['X-Priority'] = '4'

    def set_high_priority(self):
        """Définit une priorité haute au mail"""
        self.email['X-Priority'] = '2'

    def set_subject(self, subject, externe=False):
        """Définit le sujet du mail à envoyer

        Args:
            subject: Sujet du mail
            externe: Spécifique d'un mail vers l'extérieur à AOG"""
        if externe is False:
            self.subject = "[EDITIQUE] [" + self.env + "] " + subject
        else:
            self.subject = "[ASSURONE] " + subject
        self.email['Subject'] = self.subject

    def set_body(self, body):
        """Définit le corps du mail

        Args:
            body: Corps du mail"""
        self.body = body
        self.email.attach(MIMEText(self.body, 'plain'))

    def set_to(self, to):
        """Définit le destinataire principal

        Args:
            to: Destinataire
        """
        self.email.replace_header('To', to)
        if self.email['Cc'] is None:
            self.email['Cc'] = self.config["mail"]["to"]

    def set_cc(self, cc):
        """Définit le destinataire en copie

            cc: Destinataire en copie
        """
        self.email['Cc'] = cc

    def go_mail(self):
        """Envoie du mail"""
        try:
            connection = SMTP(self.address, port=int(self.port))
            connection.starttls()

            connection.send_message(self.email)
            connection.quit()
        except SMTPRecipientsRefused as ex:
            if self.logwrite is None:
                print(str(ex))
                print(self.body)
            else:
                self.logwrite.error(str(ex))


if __name__ == "__main__":
    import AOG.tools as aogtools
    appname = os.path.basename(__file__.upper())
    gc = aogtools.GeneralConfig(appname)

    mailrecap = Sendmail(gc.logwrite)

    mailrecap.set_subject("Test envoie de mail")
    gc.logwrite.info("Sujet du mail : " + mailrecap.subject)

    body = "Envoie de mail"
    mailrecap.set_body(body)
    gc.logwrite.info("body : " + mailrecap.body)
    # mailrecap.set_to("plenoir@assurone-group.com")
    mailrecap.set_high_priority()
    mailrecap.set_low_priority()
    mailrecap.add_attachment(gc.log.get_log_path())
    mailrecap.go_mail()
