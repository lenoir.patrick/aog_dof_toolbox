import os
import shutil
import sys
from os import listdir
from pathlib import Path


def recursiv_rep(dir, subdir):
    """Création des fichiers via pydoc

    :param str dir: répertoire de traitement
    :param str subdir: Répertoire de dépot des fichiers créés
    """
    files = listdir(dir)
    subrep = subdir
    for file in files:
        if file.endswith(".py"):
            try:
                cmd = "pdoc \"" + os.path.join(dir, file) + "\" --html --force"
                os.system(cmd)
            except (Exception, ):
                filename = os.path.splitext(file)[0]
                if Path(os.path.join("html", filename + ".html")).is_file():
                    print("le fichier" + filename + ".html a été géneré")
                else:
                    print("le fichier" + filename + ".html n'a pas été géneré")
                raise BaseException

            filename = os.path.splitext(file)
            os.rename("html/" + filename + '.html', "html/" + subrep + "_" + filename + '.html')

        elif Path(os.path.join(dir, file)).is_dir() and file != "venv" and file != "Django_v":
            rep = os.path.basename(file)

            recursiv_rep(os.path.join(dir, file), subrep + "-" + rep)


if __name__ == "__main__":
    rep = sys.argv[1]

    if Path(os.path.join(rep, "html")).is_dir():
        shutil.rmtree(os.path.join(rep, "html"))
    recursiv_rep(rep, os.path.basename(rep))

    print("------------------DONE-----------------------")
