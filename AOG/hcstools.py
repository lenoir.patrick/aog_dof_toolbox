#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

from datetime import datetime
import subprocess
import os
import json
import uuid
import AOG.tools as AOGtools
import AOG.constants as constants

with open(os.path.join(os.path.dirname(__file__), 'config.json')) as json_data:
    config = json.load(json_data)


def lecture_tab(op_wd, key, logwrite=None):
    """
    Récupération des paramètres du modèle depuis le fichier applis.tab.

    Cette fonction prend en paramètre le chemin d'accès à l'opWD, la clé de recherche dans le fichier applis.tab,
    et éventuellement un fichier de log. Elle retourne un dictionnaire contenant les paramètres du modèle.

    Args:
        op_wd (str): Chemin d'accès à l'opWD.
        key (str): Entrée recherchée dans le fichier applis.tab.
        logwrite (logwrite): Fichier de log (optionnel).

    Returns:
        dict: Un dictionnaire contenant les paramètres du modèle trouvés.

    Example:
        # Exemple d'utilisation de la fonction
        op_wd = "/chemin/vers/opWD"
        key = "NomDuModele"
        logwrite = open("logfile.txt", "w")
        result = lecture_tab(op_wd, key, logwrite)
        print(result)
    """
    filename = os.path.join(op_wd, "common", "tablei", "applis.tab")
    paramtab = {}
    # Chargement de la table
    if logwrite is not None:
        logwrite.debug("  Lecture du fichier : " + filename)
    # self.logwrite.info("Lecture '" + filename + "' pour récupération du paramètrage" )
    try:
        # self.logwrite.info(filename)
        fic = open(filename, "r")
    except (Exception, ):
        if logwrite is not None:
            logwrite.error("Probleme d'ouverture en lecture de la table '" + filename)
        raise
    #
    # Lecture ligne à ligne de la table avec recherche du modele
    found = False
    try:
        for line in fic:
            tab = line.strip("\n").split("\t")
            if tab[0].upper() == key.upper():
                paramtab["resdescid"] = tab[1]
                paramtab["opAppli"] = tab[3]
                paramtab["opFam"] = tab[4]
                paramtab["dataloader"] = tab[5]

                found = True
                break
    except (Exception, ):
        if logwrite is not None:
            logwrite.error("Probleme de lecture de la table '" + filename + "' avec la cle '" + key)
        raise
    #
    # Fermeture du fichier
    fic.close()
    #
    # Test si modele trouve dans la table
    if found is False:
        if logwrite is not None:
           logwrite.error("Absence du modele '" + key + "' dans la table '" + filename + "'")
    #
    # Ecriture dans la trace des parametres trouves
    if logwrite is not None:
        logwrite.debug("Parametres de modeles trouves : " + str(paramtab))
    #
    return paramtab


def run_command(commandline):
    """
    Fonction de lancement d'une commande.

    Cette fonction exécute la commande spécifiée en utilisant le module subprocess.
    Elle retourne le code de sortie de la commande, le PID du processus, la sortie standard (stdout),
    et la sortie d'erreur (stderr).

    Args:
        commandline (str): Commande à exécuter.

    Returns:
        tuple: Un tuple contenant le code de sortie (int), le PID du processus (int),
               la sortie standard (str), et la sortie d'erreur (str).

    Example:
        # Exemple d'utilisation de la fonction
        result = run_command("ls -l")
        print(result)
    """
    stdoutdata = subprocess.PIPE
    stderrdata = subprocess.PIPE

    if os.name == 'posix':
        process = subprocess.Popen(commandline, shell=True, stdout=stdoutdata, stderr=stderrdata)
    else:
        process = subprocess.Popen(commandline, stdout=stdoutdata, stderr=stderrdata)
    std_out, std_err = process.communicate()
    exit_code = process.wait()
    pid = process.pid
    return exit_code, pid, std_out, std_err


def get_nb_pages_ind(pathfile):
    """
    Retourne le nombre de pages (lignes) dans le fichier .ind.

    Cette fonction ouvre le fichier .ind spécifié, lit toutes les lignes, et retourne le nombre de lignes
    contenues dans le fichier.

    Args:
        pathfile (str): Chemin d'accès au fichier .ind.

    Returns:
        int: Nombre de lignes contenues dans le fichier .ind.

    Example:
        # Exemple d'utilisation de la fonction
        nombre_pages = get_nb_pages_ind("/chemin/vers/le/fichier.ind")
        print(nombre_pages)
    """
    with open(pathfile, encoding='utf-8') as indfile:
        text = indfile.readlines()
        return len(text)


class IndFile:
    """Classe d'écriture d'un fichier d'index pour l'usine courrier"""
    def __init__(self, appname, logwrite, indexpage=False, compo_hcs=False):
        """Constructeur de la classe de génération de fichier d'index HCS

        Args:
            appname: Nom de l'application
            logwrite: Fichier de log
            indexpage: Utilisation d'un index à la page (et non au pli)
            compo_hcs: Utilsation d'une composition HCS (NOT USED)
        """
        self.logwrite = logwrite
        self.indexpage = indexpage
        self.compo_hcs = compo_hcs

        self.rep_usine_courrier = os.path.join(config["usinecourrier"]["stockdir"])

        os.makedirs(self.rep_usine_courrier, exist_ok=True)

        self.indexfileplipath = os.path.join(self.rep_usine_courrier,
                                             config["env"] + "_PLI_" + appname + "_" + AOGtools.get_time_stamp() +
                                             ".ind")
        self.indexfilepagepath = os.path.join(self.rep_usine_courrier, config[
            "env"] + "_PAGES_" + appname + "_" + AOGtools.get_time_stamp() + ".ind")

        # Création d'un fichier d'index
        self.indexfilepli = open(self.indexfileplipath, "w")
        titre_message = constants.ET_UC + "Initialisation du fichier "
        self.logwrite.info(titre_message + self.indexfileplipath)

        # Par défaut l'utlisation d'un index à la page n'est pas effectué
        if self.indexpage is True:
            self.indexfilepage = open(self.indexfilepagepath, "w")
            self.logwrite.info(titre_message + self.indexfilepagepath)

    def __del__(self):
        """Fermeture des fichier ouverts"""
        self.indexfilepli.close()
        if self.indexpage is True:
            self.indexfilepage.close()

    # PARTIE INDEX HCS
    def get_hc_sindexfile(self, indfile):
        """Lecture du fichier d'entrée

        Args:
            indfile: Chemin d'accès au fichier"""
        # return
        indexhcs = open(indfile, "r", encoding="utf-8")
        idxline = 0
        for line in indexhcs:
            if idxline > 0:
                infos = self.init_infos()
                infos["NomFichierEntree"] = line.split("\t")[1]
                infos["NomFichierSortie"] = line.split("\t")[2]
                infos["DateTraitement"] = line.split("\t")[4]
                infos["ReferenceContrat"] = line.split("\t")[8]
                infos["Branche"] = line.split("\t")[9]
                infos["Produit"] = line.split("\t")[10]
                infos["Marque"] = line.split("\t")[11]
                infos["Compagnie"] = line.split("\t")[12]
                infos["Courtier"] = line.split("\t")[13]
                infos["TypeDocCortex"] = line.split("\t")[16]
                infos["Lotissement"] = line.split("\t")[19]
                infos["idLot"] = line.split("\t")[20]
                infos["canalDistribution"] = line.split("\t")[28]
                infos["refDistribution"] = line.split("\t")[29]
                infos["TypeImpression"] = line.split("\t")[40]
                infos["TypePage"] = line.split("\t")[41]
                infos["TypePapier"] = line.split("\t")[42]
                infos["TypeEnveloppe"] = line.split("\t")[43]
                infos["nbPages"] = line.split("\t")[45]
                infos["nbFeuilles"] = line.split("\t")[47]
                infos["refPV"] = line.split("\t")[48]
                infos["refConstat"] = line.split("\t")[49]
                infos["uuidpli"] = line.split("\t")[50]
                infos["CP"] = line.split("\t")[25]

                for item in infos:
                    line = line + str(infos[item]) + "\t"

                num_page_doc = int(line.split("\t")[0].split("_")[0])
                num_pages_pli = int(line.split("\t")[0].split("_")[1])

                self.writelinesdoc(num_pages_pli, num_page_doc, infos)
            else:
                idxline = 1

    # PARTIE INDEX ASPOSE
    def writelinepli(self, num_doc_lot, num_page_doc, line):
        """Ecriture d'un pli dans le fichier de sortie

        Args:
            num_doc_lot: Numéro du lot en cours
            num_page_doc: Numéro de la page dans le lot
            line: Ligne à écrire
        """
        # self.indexfilepli.write(str(num_doc_lot) + "_" + str(num_page_doc) + "\t" + line + "\n")
        self.indexfilepli.write(line + "\n")
        self.indexfilepli.flush()
        self.logwrite.debug(constants.ET_UC + "Pli - Ecriture de la ligne " +
                            str(num_doc_lot) + "_" + str(num_page_doc))

    def writelinepage(self, num_doc_lot, num_page_doc, line):
        """Ecriture d'un page dans le fichier de sortie

        Args:
            num_doc_lot: Numéro du lot en cours
            num_page_doc: Numéro de la page dans le lot
            line: Ligne à écrire
        """
        if self.indexpage is True:
            # self.indexfilepage.write(str(num_doc_lot) + "_" + str(num_page_doc) + "\t" + line + "\n")
            self.indexfilepage.write(line + "\n")
            self.indexfilepage.flush()
            self.logwrite.debug(
                constants.ET_UC + "Page - Ecriture de la ligne " + str(num_doc_lot) + "_" + str(num_page_doc))

    # Passer d'un index au pli/document vers un index à la page
    '''
    nb_pages_doc = nombre de feuilles dans le pli
    num_doc_lot = numero de doc dans le lot
    numPageLot = numero de page initial dans le lot
    infos = Liste des champs à traiter
    '''

    def writelinesdoc(self, nb_pages_doc, num_doc_lot, infos):
        """Ecriture des lignes d'un document

        Args:
            nb_pages_doc: Numéro de la page dans le lot
            num_doc_lot: Numéro du lot en cours
            infos: Informations à écrire
        """
        num_feuille_pli = 0
        num_page_doc = 1
        type_impression = "R"

        # print(infos["uuidpli"])
        if infos["uuidpli"] == "-" or "IDX_UNIQUE_ID" in infos["uuidpli"]:
            infos["uuidpli"] = str(uuid.uuid4())

        self.logwrite.debug("Traitement des informations du pli " + str(num_doc_lot) + " id : " + infos["uuidpli"])
        for i in range(0, nb_pages_doc):
            line = ""
            # Calcul du numéro de page et feuille pour les entêtes de ligne
            if self.indexpage is True:
                infos["numPage"] = i + 1

            if (i % 2) == 0:
                infos["TypePage"] = "recto"
                num_feuille_pli = num_feuille_pli + 1
            else:
                infos["TypePage"] = "verso"
                type_impression = "RV"

            infos["nbFeuilles"] = num_feuille_pli
            if self.indexpage is True:
                infos["numFeuille"] = num_feuille_pli

            for item in infos:
                line = line + str(infos[item]) + "\t"

            self.writelinepage(num_doc_lot, num_page_doc, line)

            num_page_doc = num_page_doc + 1

        infos["TypePage"] = "-"
        infos["TypeImpression"] = type_impression
        line = ""
        for item in infos:
            line = line + str(infos[item]) + "\t"
        # :-1 pour supprimer la dernière tabulation qui pose pb lors de l'étape de fin de journée
        self.writelinepli(num_doc_lot, num_page_doc - 1, line[:-1])

    @staticmethod
    def init_infos() -> dict:
        """Intialisation des informations à écrire

        Returns:
            Les informations par défaut
        """
        infos = {"NomFichierEntree": "-",
                 "NomFichierSortie": "-",
                 "DateTraitement": "-",
                 "DateImpression": datetime.now().strftime('%Y-%m-%d'),
                 "ReferenceContrat": "-",
                 "Branche": "-",
                 "Produit": "-",
                 "Marque": "-",
                 "Compagnie": "-",
                 "Courtier": "-",
                 "TypeDocCortex": "-",
                 "Lotissement": "-",
                 "idLot": "-",
                 "CP": "-",
                 "canalDistribution": "-",
                 "refDistribution": "-",
                 "TypeImpression": "-",
                 "TypePage": "-",
                 "TypePapier": "-",
                 "TypeEnveloppe": "-",
                 "nbPages": "-",
                 "nbFeuilles": "-",
                 "refPV": "-",
                 "refConstat": "-",
                 "uuidpli": "-"}
        return infos

    #
    def init_files(self) -> dict:
        """Initialisation des index = les mêmes pour toutes les applications

        Returns:
            Les informations par défaut
        """
        infos = self.init_infos()
        line = ""
        for item in infos:
            line = line + item + "\t"
        self.indexfilepli.write(line[:-1] + "\n")

        if self.indexpage is True:
            self.indexfilepage.write(line + "\n")
        return infos


class PndFile:
    def __init__(self, appname, logwrite):
        """Constructeur de la classe d'index pour les documents PND

        Args:
            appname: Nom de l'application en cours
            logwrite: Fichier de log
        """
        self.logwrite = logwrite
        self.indexfileplipath = os.path.join(config["usinecourrier"]["stockdir"],
                                             config["env"] + "_PND_" + appname + "_" + AOGtools.get_time_stamp() +
                                             ".ind")

        self.indexfilepli = open(self.indexfileplipath, "w")
        self.logwrite.info(constants.ET_UC + "Initialisation du fichier " + self.indexfileplipath)

    @staticmethod
    def init_infos() -> dict:
        """Intialisation des informations à écrire

        Returns:
            Les informations par défaut
        """
        infos = {"TypeDocCortex": "AE",
                 "ReferenceContrat": "-",
                 "DatePND": "-"}
        return infos

    def init_files(self) -> dict:
        """Initialisation des fichiers à écrire

        Returns:
            Les informations par défaut
        """
        infos = self.init_infos()
        line = ""
        for item in infos:
            line = line + item + "\t"
        self.indexfilepli.write(line[:-1] + "\n")

        return infos

    def writelinepli(self, line):
        """Ecriture d'un pli dans le fichier de sortie

        Args:
            line: Ligne à écrire
        """
        # self.indexfilepli.write(str(num_doc_lot) + "_" + str(num_page_doc) + "\t" + line + "\n")
        self.indexfilepli.write(line + "\n")
        self.indexfilepli.flush()
        self.logwrite.debug(constants.ET_UC + "Pli - Ecriture de la ligne " + line)


class LicenceHCS:
    """
    Fonctions de gestion de la licence HCS.

    Cette classe fournit des fonctionnalités pour la gestion de la licence HCS, notamment
    la récupération d'informations sur la consommation de licences en production.

    Attributes:
        __op_install_dir (str): Le chemin vers le répertoire d'installation d'OP.
        __report_dir (str): Le chemin vers le répertoire des rapports dans le répertoire d'installation d'OP.
        logwrite: Le fichier de log pour enregistrer les messages.

    Methods:
        infos_licences_consommees(): Retourne la consommation de la licence HCS en production.
        __avant_derniere_ligne_du_dernier_fichier(): Récupère l'avant-dernière ligne du dernier fichier modifié.
        __dernier_fichier_modifie(): Retourne le nom du dernier fichier modifié dans le répertoire des rapports.

    Example:
        # Exemple d'instanciation de la classe
        instance = LicenceHCS("/chemin/vers/op/installation", log_file)
    """
    def __init__(self, logwrite=None):
        op_install_dir = config["hcs"]["opInstallDir_Platform"]
        self.__op_install_dir = op_install_dir
        self.__report_dir = os.path.join(self.__op_install_dir, "common", "config", "report")
        self.logwrite = logwrite

    def __dernier_fichier_modifie(self):
        """
        Retourne le nom du dernier fichier modifié dans le répertoire spécifié.

        Cette méthode liste tous les fichiers dans le répertoire, les trie par date de modification (du plus récent
        au plus ancien), puis retourne le nom du premier fichier de la liste (le plus récent).
        Si le répertoire est vide, elle retourne None.

        Returns:
            str or None: Nom du dernier fichier modifié, ou None si aucun fichier n'est trouvé.

        Example:
            # Exemple d'utilisation de la méthode
            instance = MaClasse()
            resultat = instance.__dernier_fichier_modifie()
            print(resultat)
        """
        # Liste tous les fichiers dans le répertoire
        fichiers = [f for f in os.listdir(self.__report_dir) if os.path.isfile(os.path.join(self.__report_dir, f))]

        # Trie les fichiers par date de modification (du plus récent au plus ancien)
        fichiers_tries = sorted(fichiers, key=lambda f: os.path.getmtime(os.path.join(self.__report_dir, f)),
                                reverse=True)

        # Récupère le premier fichier (le plus récent)
        dernier_fichier = fichiers_tries[0] if fichiers_tries else None

        return dernier_fichier

    def __avant_derniere_ligne_du_dernier_fichier(self):
        """
        Récupère l'avant-dernière ligne du dernier fichier modifié.

        Cette méthode utilise la fonction __dernier_fichier_modifie() pour obtenir le nom du dernier fichier modifié,
        puis ouvre ce fichier, lit toutes les lignes, et retourne l'avant-dernière ligne si le fichier contient
        au moins deux lignes. Sinon, elle retourne un message indiquant que le fichier ne contient pas assez de lignes.

        Returns:
            str: Avant-dernière ligne du dernier fichier modifié, ou un message d'erreur.

        Example:
            # Exemple d'utilisation de la méthode
            instance = MaClasse()
            resultat = instance.__avant_derniere_ligne_du_dernier_fichier()
            print(resultat)
        """

        dernier_fichier = self.__dernier_fichier_modifie()

        if dernier_fichier:
            chemin_dernier_fichier = os.path.join(self.__report_dir, dernier_fichier)
            with open(chemin_dernier_fichier, 'rb') as fichier:
                lignes = fichier.readlines()
                if len(lignes) >= 2:
                    avant_derniere_ligne = lignes[-2]
                    return avant_derniere_ligne
                else:
                    if self.logwrite is not None:
                        self.logwrite.warning("Le fichier ne contient pas assez de lignes pour avoir une avant-dernière ligne.")
                    return 0
        else:
            if self.logwrite is not None:
                self.logwrite.warning("Aucun fichier trouvé dans le répertoire.")
            return 0

    def infos_licences_consommees(self):
        """
        Retourne la consommation de la licence HCS en production.

        Cette fonction récupère l'avant-dernière ligne du dernier fichier modifié,
        la décode en UTF-8, la divise en colonnes en utilisant le caractère de tabulation comme séparateur,
        et retourne l'information de consommation de licence qui se trouve à la 6e colonne.

        Returns:
            str: Information de consommation de licence HCS en production.

        Example:
            # Exemple d'utilisation de la fonction
            instance = MaClasse()
            resultat = instance.infos_licences_consommees()
            print(resultat)
        """
        if self.logwrite is not None:
            self.logwrite.info("Vérification de l'opInstlalDir : " + self.__op_install_dir)
        if os.path.isdir(self.__report_dir) is False:
            if self.logwrite is not None:
                self.logwrite.warning("L'environnement n'a pas de serveur de licence")
            return 0
        else:
            avant_derniere_ligne = self.__avant_derniere_ligne_du_dernier_fichier()
            if self.logwrite is not None:
                self.logwrite.info(f"L'avant-dernière ligne du dernier fichier modifié est : {avant_derniere_ligne}")

            ligne_decodee = avant_derniere_ligne.decode('utf-8').strip()

            # Divise la ligne en utilisant le caractère de tabulation comme séparateur
            colonnes = ligne_decodee.split('\t')

            # Information de consommation de licence
            info_conso = colonnes[5]

            return info_conso

    def get_version_termes(self):
        opwd = config["hcs"]["opWD_termes"]
        return lecture_tab(opwd, "termes", self.logwrite)


if __name__ == "__main__":
    import AOG.tools as aogtools
    appname = os.path.basename(__file__.upper())
    gc = aogtools.GeneralConfig(appname)

    gc.logwrite.info(lecture_tab("D:\\referentielEditique", "termes", gc.logwrite))
    gc.logwrite.info(run_command('ping www.google.fr'))

    ind = IndFile(appname, gc.logwrite)
    ind.init_files()
    ind.writelinesdoc(1, 1, ind.init_infos())

    conso = LicenceHCS(gc.logwrite)
    consom = conso.infos_licences_consommees()
    gc.logwrite.info("Licence consommée : " + str(consom))

    conso.get_version_termes()
