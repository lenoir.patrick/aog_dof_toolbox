#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
import os
import json
import AOG.constants as constants
from AOG.logfile import LogFileException


def upload_ftp(workingdir, logwrite, debug, nbdocs, fileformat=constants.ZIP_EXT):
    upload = UploadFtp(workingdir, logwrite=logwrite, debug=debug, pathcmd=os.path.dirname(__file__))
    upload.setfiles(nbdocs, fileformat, prefix="ASSURONE")
    return upload


class UploadFtpException(Exception):
    def __init__(self, ex, logwrite):
        self.message = "Erreur lors du transfert FTP"
        self.ex = ex
        self.logwrite = logwrite

    def __str__(self):
        self.logwrite.error(self.message + " | " + str(self.ex))
        return self.message


class UploadFtp:
    def __init__(self, inputdir, protocol=None, login=None, password=None, address=None, port=None,
                 logwrite=None, debug=True, pathcmd=None):
        """Constructeur de la classe UploadFtp.

        :param str inputdir: Chemin d'accès au fichier à traiter
        :param str protocol: Protocole utilisé
        :param str login: Login utilisé
        :param str password: Password
        :param str address: Adresse ftp
        :param int port: port
        :param logwrite logwrite: Instance de log
        :param bool debug: Mode debug
        :param str pathcmd: Chemin d'accès au fichier de commande
        """
        if logwrite is None:
            raise LogFileException

        self.logwrite = logwrite
        self.debug = debug

        self.inputdir = inputdir
        with open(os.path.join(os.path.dirname(__file__), 'config.json')) as json_data:
            config = json.load(json_data)

        if protocol is None:
            protocol = config["ftp"]["protocol"]
        if login is None:
            login = config["ftp"]["login"]
        if password is None:
            password = config["ftp"]["password"]
        if address is None:
            address = config["ftp"]["address"]
        if port is None:
            port = config["ftp"]["port"]

        self.execfile = os.path.join(pathcmd, "commandftp.txt")
        self.protocol = protocol
        self.url = protocol + "://" + login + ":" + password + "@" + address + ":" + str(port)
        self.logwrite.info(constants.ET_TRANSFERT + "Initialisation de la connexion FTP : " + self.url)

    def setfiles(self, nb_docs, fileformat=".pdf", prefix=""):
        """Préparation des informations avant transfert (création fichier WinSCP, fichiers à transférer).

        :param int nb_docs: Nombre de fichiers à traiter
        :param str fileformat: Format de fichiers à traiter
        :param str prefix: Préfixe des fichiers à traiter
        """
        self.logwrite.info("Préparation des fichiers à transférer")
        with open(os.path.join(self.inputdir, self.protocol + ".txt"), "w", encoding='utf-8') as sftpfile:
            sftpfile.write("option echo off \n")
            sftpfile.write("option batch on \n")
            sftpfile.write("option confirm on \n")
            if self.debug is False:
                sftpfile.write(
                    "open " + self.url + "\n")

            for file in os.listdir(self.inputdir):
                if file.endswith(fileformat) and file.startswith(prefix):
                    self.logwrite.info("   Ajout du fichier : " + file)
                    file = os.path.join(self.inputdir, file)
                    sftpfile.write("put " + file + " -resumesupport=off\n")
            sftpfile.write("exit\n")

        cmd = "\"C:\\Program Files (x86)\\WinSCP\\WinSCP.com\""
        cmd = cmd + " /script=" + os.path.join(self.inputdir, self.protocol + ".txt")
        cmd = cmd + " /log=" + os.path.join(self.inputdir, self.protocol + ".log")

        fileexec = open(self.execfile, "a")
        fileexec.write(cmd + ";" + str(nb_docs) + "\n")
        fileexec.close()
        self.logwrite.info(constants.ET_TRANSFERT + "Fin de la préparation des fichiers à traiter")

    def uploadfiles(self, prod=True):
        """Transfert des fichiers

        :param bool prod: Mode production (active uniquement en production)
        """
        if prod is True:
            self.logwrite.info("Début de l'envoie FTP")

            fileexec = open(self.execfile, "r")
            try:
                for lines in fileexec:
                    cmd = lines.split(";")[0]

                    self.logwrite.info(constants.ET_TRANSFERT + "Execution de la commande : " + cmd)

                    if self.debug is False:
                        retour = os.system(cmd)
                        self.logwrite.info(constants.ET_TRANSFERT + "Retour de WinSCP : " + str(retour))

                self.logwrite.info(constants.ET_TRANSFERT + "Fin de l'étape de transfert FTP")
            except UploadFtpException as ex:
                raise UploadFtpException(ex, self.logwrite)
            except (Exception, ):
                self.logwrite.error(constants.ET_TRANSFERT + "Erreur générale lors traitement FTP")
            fileexec.close()

        os.remove(self.execfile)


if __name__ == "__main__":
    import AOG.tools as aogtools
    appname = os.path.basename(__file__.upper())
    gc = aogtools.GeneralConfig(appname)
    upload = UploadFtp(gc.get_tmp_dir(), logwrite=gc.logwrite, pathcmd=os.path.dirname(__file__))
    upload.setfiles(1, fileformat=".zip", prefix="ASSURONE")
    upload.uploadfiles(False)
