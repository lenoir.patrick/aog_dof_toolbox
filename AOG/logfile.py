#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
import datetime
import json
import os
import logging
import logging.handlers
from socket import gethostname


LOGLEVELS = {"debug": logging.DEBUG, "info": logging.INFO, "warn": logging.WARNING, "warning": logging.WARNING,
             "error": logging.ERROR}
LEVELNAMES = {50: "CRITICAL", 40: "ERROR", 30: "WARNING", 20: "INFO", 10: "DEBUG", 0: "NOTSET"}


class LogFileException(Exception):
    def __init__(self, m="Pas de fichier de log pour ce traitement"):
        self.message = m

    def __str__(self):
        print(self.message)
        return self.message


class Logfile:
    def __init__(self, name=None):
        """Constructeur de la classe de log

        Args:
            name: Nom de l'application utilisée
        """
        with open(os.path.join(os.path.dirname(__file__), 'config.json')) as json_data:
            self.config = json.load(json_data)

        now = datetime.datetime.today()
        date = now.strftime("%Y%m%d")
        heure = now.strftime("%H%M%S")

        logdir = self.config["dirs"]["logdir"]

        # Cas général sur les serveurs de traitements
        if os.path.isdir(logdir) is True:
            self._logdir = logdir
        else:
            try:
                os.makedirs(logdir)
                self._logdir = logdir
            except (Exception, ):
                self._logdir = os.getcwd()

        if name is None:
            self._rootlogname = os.path.basename(__file__).replace(".py", "")
        else:
            self._rootlogname = name
        logname = self._rootlogname + "_" + date + "_" + heure + ".log"
        self._logpath = os.path.join(self._logdir, logname)

    def get_log_path(self):
        """Retourne la localisation du fichier de log

        Returns:
            Fichier de log
        """
        return self._logpath

    def delete_old_logs(self, _log):
        """Supprimer les anciennes occurences de log

        :param logwrite _log: Nouvelle instance de log"""
        nb_save_logs = 0
        for logs in sorted(os.listdir(self._logdir), reverse=True):
            if self._rootlogname in logs:
                if nb_save_logs > 5:
                    try:
                        os.remove(os.path.join(self._logdir, logs))
                        _log.debug("suppression du fichier : " + logs)
                    except (Exception, ):
                        pass
                nb_save_logs = nb_save_logs + 1

    def create_logger(self, level='info', console=True, debugparams=None):
        """Création de l'instance de logger

        :param str level: Niveau d'information demandé
        :param bool console: Affichage dans la console
        :param dict debugparams: Paramètres du debugger
        """
        """Configure program logger."""
        if level not in LOGLEVELS:
            raise LogFileException("Mauvais niveau de paramétrage (debug|info|warn|error)")

        level = LOGLEVELS[level]
        # This is the "root" logger for CNAM
        _log = logging.getLogger("assurone-group")

        format_str = '%(asctime)-15s [%(levelname)-8s]'

        if level == logging.DEBUG:
            format_str += '%(filename)s:%(lineno)4d (%(funcName)s) -'
        format_str += ' %(message)s'

        logformatter = logging.Formatter(format_str)
        # CONSOLE LOGGING
        if console:
            streamhandler = logging.StreamHandler()
            streamhandler.setLevel(level)
            streamhandler.setFormatter(logformatter)
            _log.addHandler(streamhandler)
        # FILE LOGGING
        filehandler = logging.handlers.RotatingFileHandler(self._logpath, maxBytes=10485760,
                                                           backupCount=5)  # 10 MB rotating
        filehandler.setLevel(level)
        filehandler.setFormatter(logformatter)
        _log.addHandler(filehandler)
        #
        _log.setLevel(level)
        #
        _log.debug('Logging initialized to level : ' + str(LEVELNAMES[level]))
        _log.info('Serveur  : ' + str(gethostname()))
        _log.info('Fichier  : ' + str(self._logpath))
        if debugparams is not None:
            for item in debugparams:
                _log.info(item.ljust(8) + ' : ' + str(debugparams[item]))
        #
        self.delete_old_logs(_log)
        return _log


if __name__ == "__main__":
    import AOG.tools as aogtools
    appname = os.path.basename(__file__.upper())
    gc = aogtools.GeneralConfig(appname)
