#!/usr/bin/env python
"""Django_v's command-line utility for administrative tasks."""
import os
import sys

import smtplib
import traceback


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Django_v.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django_v. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    with open('D:/scripts/AOG_DOF_Toolbox/Django_v/pid.txt', 'w') as f:
        f.write(str(os.getpid()))
    main()
