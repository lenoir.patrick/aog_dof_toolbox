## Version
`Django 4.1.1` sur `Python3.10`
## Architechture
├── AOG_DOF_Toolbox
│   ├── Django_v
│   │   ├── Django_v
│   │   │   ├── settings.py
│   │   │   ├── iews.py
│   │   │   ...
│   │   ├── static
│   │   │   ├── images
│   │   │   ├── style
│   │   │   │   ├── style.css
│   │   ├── templates
│   │   ├── tests
│   │   ├── manage.py
        
## Installation et execution

#### Clonnage du repertoire django
#### Installation du contennu du fichier requirement.txt
```sh
$ pip install -r requirements.txt
```
#### Lancer le serveur de dev avec la commande :
```sh
$ python manage.py runserver

November 07, 2022 - 11:47:51
Django version 4.1.2, using settings 'Django_v.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CTRL-BREAK.
```

#### Le site sera donc accessible depuis 
```sh
$ http://127.0.0.1:8000/
```
Le serveur ne sera plus accessible si l'on éxécute une commande de kill le terminal.

# Build and Test
Pour executer les testes, il suffit de se positionner dans le repertoire racine de django et de lancer la commande
```sh
$ pytest
```
Les fichier media qui sont collecté sur django seront stocké dans un repertoire `media`, et ça en configurant le fichier  `settings.py`

```python
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
```
Les fichier utilisés dans le site tels que la feille de style ou les images sont stocké dans un repertoire `static`

```python
STATIC_URL = 'static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static')
]
```

Le module django utilise egalement le language ``jinja`` pour creer des boucle et des conditions dans les fichiers `HTML`
```jinja
{% for file in files %}
     <div>
        <a href="./static/html/{{ file }}">{{ file }}</a>
     </div>
{% endfor %}
```


### static files
- la configuration des fichiers statique se fait dans le script de configuration `settings.py`
- lien
```python
STATIC_URL = 'static/'
```
  - chemin vers le repertoire static
```python
STATICFILES_DIRS = ("D:\\scripts\\test_dihia\\Django_v\\static",)
```

- il suffit ensuite d'executer la commande `collectstatic` (ou `collectstatic --noinput` pour que le terminal ne rende pas la main lors de la confirmation).
- il faut egalement avoir le parametre `DEBUG` set a False pour que la commande s'execute correctement.

### Sources / Documentations
**https://stackoverflow.com/questions/20398600/django-runserver-not-serving-static-files-in-development**
**https://docs.djangoproject.com/en/4.1/**