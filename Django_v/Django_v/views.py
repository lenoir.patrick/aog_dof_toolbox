import os
from os import listdir
from zipfile import ZipFile
import zipfile
from calendar import monthrange
from datetime import datetime
import json
import threading
import socket
import sys
import mysql.connector
import xml.etree.ElementTree as ET

from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.templatetags.static import static
from django.shortcuts import render

from mysql.connector import IntegrityError

from .page_calcul.api_request import do_sefas_request
from .page_calcul.calcul import Calculette
from .settings import BASE_DIR


tmp_dir = "D:\\temp\\django_recherche"

config_json = os.path.join(BASE_DIR, "configuration", "config.json")
with open(config_json) as jsonFile:
    data = json.load(jsonFile)

sys.path.append("D:\\scripts\\AOG_DOF_Toolbox")
import AOG.tools as aogtools
from AOG.hcstools import LicenceHCS

appname = os.path.basename(__file__.upper())
gc = aogtools.GeneralConfig(appname)

HOSTNAME = socket.gethostname()


def connexion_sql():
    connexion = mysql.connector.connect(option_files='D:/scripts/AOG_DOF_Toolbox/Django_v/Django_v/config.cnf')
    return connexion


class ScriptThread(threading.Thread):
    def __init__(self, path):
        threading.Thread.__init__(self)
        self.path = path

    def run(self):
        p = self.path.replace("\\", "/")
        os.system(p)


class FileDate:
    def __init__(self, date):
        self.date = date
        self.files = []


class DataDict:
    def __init__(self, theme, item, config_errors=None):
        self.theme = theme
        self.script_path = item["script_path"]
        self.script_name = item["script_name"]
        self.script_image = item["script_image"]
        self.periodicite = item["periodicite"]
        self.lst_errors = ""
        self.lst_warnings = ""
        self.lst_ok = ""
        try:
            self.last_result = config_errors["result"]
            self.date_traitement = config_errors["date_traitement"]
            self.date_diff = config_errors["date_diff"]
            for item in config_errors["lst_errors"]:
                self.lst_errors = self.lst_errors + item + "<br>"

            for item in config_errors["lst_warnings"]:
                self.lst_warnings = self.lst_warnings + item + "<br>"

            for item in config_errors["lst_ok"]:
                 self.lst_ok = self.lst_ok + item + "<br>"

        except (Exception, ):
            self.last_result = None


class ImgDict:
    """
    Classe d'affichage des infos du prestataire et de l'application souhaitée
    """
    def __init__(self, theme, image_path, new_theme):
        self.theme = theme
        self.image_path = image_path
        self.new_theme = new_theme


class SrvDict:
    """
    Classe d'affichage des environnements de travail
    """
    def __init__(self, env, infos):
        self.env = env
        self.adresse_ip = infos["adresse_ip"]
        self.port = infos["port"]
        self.result = infos["result"]
        self.description = infos["description"]

class EnvDict:
    def __init__(self, env):
        self.env = env["id"]
        self.nom = env["nom"]
        self.adresse_ip = env["adresse_ip"]
        self.description = env["description"]


class QueryFound:
    def __init__(self, query):
        self.query = query
        self.found = False


class FileOpen:
    def __init__(self, file):
        self.file = file
        self.linerr = ""


def home(request):
    files = listdir(os.path.join(BASE_DIR, "static\\html"))
    return render(request, "documentation.html", context={"files": files})


def search_bd(request):
    if request.POST.get('rech_bd', default=None) is not None and request.POST.get('rech_bd', default=None) != "":
        # stocké les données d'entré (numero de contrat recherché)
        query = request.POST.get('rech_bd', default=None)
        typedoc = request.POST.get('type', default=None)
        gamme_selectionnee = request.POST.get('gamme', default=None)
        jours = request.POST.get('jours', default=None)

        gamme = data["gamme"].split(",")
        type_gamme = data["type"].split(",")

        # print(type)
        # print(gamme)
        # print(jours)
        plis = []

        if query == "*":
            query = ""

        sql_plus = ""
        if typedoc != "tous":
            if typedoc == "GESTION":
                sql_plus = sql_plus + " AND PP.type NOT IN (\"LC\", \"LR\", \"TERMES\") "
            if typedoc == "GESTION+LR":
                sql_plus = sql_plus + " AND PP.type NOT IN (\"TERMES\", \"LC\") "
            elif typedoc == "LR":
                sql_plus = sql_plus + " AND PP.type = \"LR\" "
            elif typedoc == "LC":
                sql_plus = sql_plus + " AND PP.type = \"LC\" "
            elif typedoc == "TERMES":
                sql_plus = sql_plus + " AND PP.type = \"TERMES\" "
            elif typedoc == "CP":
                sql_plus = sql_plus + " AND UC.TypeDocCortex = \"CP\" "
            elif typedoc == "CV":
                sql_plus = sql_plus + " AND UC.TypeDocCortex IN (\"CVPROV\", \"CVFACS\", \"CVDEFINITIVE\")"
        if gamme_selectionnee != "tous":
            sql_plus = sql_plus + " AND UC.Branche = '" + gamme_selectionnee + "'"
        print(sql_plus)
        # ss_type = ""
        # cout_total = ""
        conn = connexion_sql()
        cursor = conn.cursor()
        # cursor.execute("SELECT * FROM usine_courrier WHERE ReferenceContrat = %s", (query,))

        requete = "SELECT UC.NomFichierEntree, UC.NomfichierSortie, UC.DateTraitement, UC.DateImpression, UC.ReferenceContrat, UC.Branche, UC.Produit, UC.Marque, UC.Compagnie, UC.TypeDocCortex, UC.pnd, PP.type, PP.ss_type, UC.refDistribution, UC.Recommande ,(UC.pxenv + UC.pxpapier + UC.pxconstat + UC.pxpv) AS Somme_Consommables, (UC.pxtraitement + UC.pxpnd) AS Somme_Traitement, UC.pxaffranchissement, (UC.pxenv + UC.pxpapier + UC.pxconstat + UC.pxpv + UC.pxtraitement + UC.pxaffranchissement + UC.pxpnd) AS Somme_Total, UC.canalDistribution FROM usine_courriers_editique.usine_courrier as UC JOIN usine_courriers_editique.production_papier as PP ON UC.production_papier_ID = PP.ID WHERE ReferenceContrat LIKE '%" + query + "%' AND (UC.DateTraitement >= DATE_SUB(NOW(), INTERVAL " + jours + " MONTH) OR  UC.DateImpression >= DATE_SUB(NOW(), INTERVAL " + jours + " MONTH)) " + sql_plus + " ORDER BY UC.DateTraitement DESC, UC.ReferenceContrat LIMIT 1000"
        # print(requete)

        cursor.execute(requete)
        results = cursor.fetchall()
        # Parcourir les résultats
        for row in results:
            plis.append(row)

        return render(request, "resultats_bd.html", context={"plis": plis, "gamme": gamme, "type": type_gamme})


@csrf_exempt
def search(request):
    global chemin_archives
    results = []
    annee = ""
    mois = ""
    jour = ""
    query = request.POST.get('num_contact', default=None)
    if request.POST.get('rech', default=None) is not None:
        # Chemin du répertoire principal
        chemin_base = data["path_archive"]

        # Obtention de la date d'aujourd'hui
        date_actuelle = datetime.now()

        # Vérification si le mois en cours a déjà commencé
        jour_en_cours = date_actuelle.day
        print(jour_en_cours)
        # Parcourir les jours du mois en cours
        for i in range(1, jour_en_cours + 1):
            print(jour_en_cours + 1)

            # Calcul de la date pour le jour à parcourir dans le mois en cours
            date_jour = datetime(date_actuelle.year, date_actuelle.month, i)
            annee = str(date_jour.year)
            mois = str(date_jour.month).zfill(2)  # Remplir avec un zéro si nécessaire
            jour = str(date_jour.day).zfill(2)  # Remplir avec un zéro si nécessaire

            nom_archive = f"ASSURONE_{annee}-{mois}-{jour}.zip"
            # Chemin complet du dossier d'archives pour le jour actuel
            chemin_archive = os.path.join(chemin_base, annee, mois, jour, nom_archive)
            # Recherche de fichiers PDF dans le dossier d'archives du jour actuel
            if os.path.exists(chemin_archive):
                nouveau_dossier = tmp_dir
                os.makedirs(nouveau_dossier, exist_ok=True)
                print(chemin_archive)
                # Extraction des fichiers PDF de l'archive
                with zipfile.ZipFile(chemin_archive, 'r') as archive:
                    for fichier in archive.namelist():
                        if fichier.endswith(".pdf") and query in fichier:
                            # Extraction du fichier PDF vers le nouveau dossier
                            archive.extract(fichier, path=nouveau_dossier)
                            # results.append(fichier)
                            detail_fichier = {'type':'GESTION','nom': fichier, 
                                              'date_emission': fichier.split("-")[-2],
                                              'date_envoi': str(annee) + str(mois) + str(jour),
                                              'num_contrat': fichier.split("-")[2],
                                               'location': os.path.join(tmp_dir, fichier)}
                            results.append(detail_fichier)
                            # print(detail_fichier)


        # Calcul du mois précédent

        jours_a_parcourir_mois_precedent = 7 - len(os.listdir(os.path.join(chemin_base, annee, mois)))
        mois_precedent = date_actuelle.month - 1 if date_actuelle.month > 1 else 12
        annee_precedente = date_actuelle.year if date_actuelle.month > 1 else date_actuelle.year - 1
        dernier_jour_mois_precedent = monthrange(annee_precedente, mois_precedent)[1]

        # Parcourir les jours restants du mois précédent
        for i in range(dernier_jour_mois_precedent - jours_a_parcourir_mois_precedent, dernier_jour_mois_precedent):
            print("ee")
            # Calcul de la date pour le jour à parcourir dans le mois précédent
            date_jour = datetime(annee_precedente, mois_precedent, i)
            annee = str(date_jour.year)
            mois = str(date_jour.month).zfill(2)  # Remplir avec un zéro si nécessaire
            jour = str(date_jour.day).zfill(2)  # Remplir avec un zéro si nécessaire

            nom_archive = f"ASSURONE_{annee}-{mois}-{jour}.zip"

            # Chemin complet de l'archive
            chemin_archive = os.path.join(chemin_base, annee, mois, jour, nom_archive)

            # Recherche de fichiers PDF dans le dossier d'archives du jour actuel
            if os.path.exists(chemin_archive):
                nouveau_dossier = tmp_dir
                os.makedirs(nouveau_dossier, exist_ok=True)

                # Extraction des fichiers PDF de l'archive
                with zipfile.ZipFile(chemin_archive, 'r') as archive:
                    for fichier in archive.namelist():
                        if fichier.endswith(".pdf") and query in fichier:
                            # Extraction du fichier PDF vers le nouveau dossier
                            archive.extract(fichier, path=nouveau_dossier)
                            # results.append(fichier)
                            detail_fichier = {'type':'GESTION','nom': fichier, 
                                              'date_emission': fichier.split("-")[-2],
                                              'date_envoi': str(annee) + str(mois) + str(jour),
                                              'num_contrat': fichier.split("-")[2],
                                              'location': os.path.join(tmp_dir, fichier)}
        #                     results.append(detail_fichier)
        path = ""
        list_query = ""
        dire = ("\\\\10.128.3.120\\cortex\\depots\\poste\\courriers_recommandes")
        listedir = os.listdir(dire)
        listnew = []
        for item in listedir:
            test = os.path.join(dire, item)
            if os.path.isdir(test):
                listnew.append(item)

        # Créez une liste de tuples (nom du fichier, date de modification)
        file_info_list = [(f, os.path.getmtime(os.path.join(dire, f))) for f in listnew]

        # Triez la liste par date de modification (du plus ancien au plus récent)
        file_info_list.sort(reverse=True, key=lambda x: x[1])

        goodlist = file_info_list[0:21]

        for repertoire in goodlist:
            searchdir = os.path.join(dire, repertoire[0])
            for item in os.listdir(searchdir):
                if query in item:
                    detail_fichier = {'type':'LR',
                                      'nom': item, 
                                      'date_emission': repertoire[0],
                                      'date_envoi': '',
                                      'num_contrat': query,
                                      'location': os.path.join(searchdir, item)}
                    results.append(detail_fichier)    



        return render(request, "resultats.html", context={"files": results, "list_query": list_query, "path": path, "hostname": HOSTNAME})

    else:
        if request.POST.get('archive', default=None) is not None:
            path_archive = ""
            # for root, dirs, files in os.walk("//10.128.3.120/cortex/prod/pdf/ADM/DEMANDE_IMPRESSION/prevoir"):
            for root, dirs, files in os.walk(data["path_archive"]):
                for name in dirs:
                    folder_path = os.path.join(root, name)
                    if os.path.isdir(folder_path):
                        path_archive = folder_path

            zip_obj = ZipFile(path_archive + '/AOG_REJEU.zip', 'w')
            for file_child in listdir(tmp_dir):
                zip_obj.write(tmp_dir + file_child, arcname=file_child)

            zip_obj.close()
            path = "dans le dossier du jours, AOG_REJEU.zip"
            return render(request, "popup_archive.html", context={"path": path})
    print("ok")

    return render(request, "recherche.html", context={"hostname": HOSTNAME})


def upload(request):
    # Chemin du dossier
    chemin_dossier = tmp_dir
    os.makedirs(chemin_dossier, exist_ok=True)

    # Parcourir les fichiers du dossier
    for fichier in os.listdir(chemin_dossier):
        chemin_fichier = os.path.join(chemin_dossier, fichier)
        if os.path.isfile(chemin_fichier):
            os.remove(chemin_fichier)

    gamme = data["gamme"].split(",")
    type_gamme = data["type"].split(",")
    return render(request, 'recherche.html', context={"gamme": gamme, "type": type_gamme})


def logpaths(request):
    # with open(config_json) as jsonFile:
    #     data = json.load(jsonFile)
    files = listdir(data["path_log"])
    list_file = []
    for file in files:
        ff = FileOpen(file)
        f = open(data["path_log"] + ff.file).readlines()
        for line in f:
            if "[ERROR" in line:
                ff.linerr = ff.linerr + line + "\n"
        list_file.append(ff)
    return render(request, "fichier_log.html", context={"files": files, "objects": list_file})


@csrf_exempt
def scripts_run(request):
    data_list = []
    theme_list = []
    theme_image_paths = []

    with open(os.path.join(gc.get_tmp_dir(), "django_interface", "logs.json")) as json_data:
        config_errors = json.load(json_data)

    # with open(config_json) as jsonFile:
    #     data = json.load(jsonFile)
        for theme in data:
            if isinstance(data[theme], list):
                try:
                    newtheme = " - " + theme.split("_")[0]
                    newappli = theme.split("_")[1]
                except (Exception, ):
                    newtheme = ""
                    newappli = theme.split("_")[0]

                # Recherche des infos d'exploitation
                theme_list.append(theme)
                for item in data[theme]:
                    try:
                        config_err = config_errors[item["config_errors"]]
                    except (Exception, ):
                        config_err = None

                    data_list.append(DataDict(theme, item, config_err))

                theme_image_path = static('images/' + newappli + '.png')
                theme_image_paths.append(ImgDict(theme, theme_image_path, newtheme))

    # TODO : Faire une fonction pour réutiliser cette partie sur toutes les pages
    liste_env = []
    with open("D:\\scripts\\AOG_DOF_Toolbox\\Django_v\\configuration\\serveurs.json") as jsonFile:
        environnements = json.load(jsonFile)
        # Parcours des environnements
        for environnement in environnements:

            if environnement == "envs":
                for item in environnements[environnement]:
                    liste_env.append(EnvDict(item))

    context={
        "data": data_list,
        "themes": theme_list,
        "hostname": HOSTNAME,
        "theme_image_paths": theme_image_paths,
        "liste_env": liste_env}

    return render(request, "scripts.html", context)

def etat_serveurs(request):
    liste_srv = []
    liste_env = []
    with open("D:\\scripts\\AOG_DOF_Toolbox\\Django_v\\configuration\\serveurs.json") as jsonFile:
        environnements = json.load(jsonFile)
        # Parcours des environnements
        for environnement in environnements:

            if environnement != "envs":
                for item in environnements[environnement]:
                    adresse_serveur = item['adresse_ip']
                    port_serveur = item['port']
                    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                    try:
                        # Essayez de vous connecter au serveur
                        client_socket.connect((adresse_serveur, port_serveur))
                        # print(f"Le serveur sur {adresse_serveur}:{port_serveur} répond.")
                        item['result'] = True
                    except ConnectionRefusedError:
                        # print(f"Le serveur sur {adresse_serveur}:{port_serveur} ne répond pas.")
                        item['result'] = False
                    except (Exception, ):
                        # print(f"Une erreur s'est produite : {e}")
                        item['result'] = False
                    finally:
                        # Fermez la connexion
                        client_socket.close()
                    liste_srv.append(SrvDict(environnement, item))
            else:
                for item in environnements[environnement]:
                    liste_env.append(EnvDict(item))

    conso = LicenceHCS()
    print(conso.infos_licences_consommees())
    print(conso.get_version_termes())
    context={
        "hostname": HOSTNAME,
        "liste_env": liste_env,
        "liste_srv": liste_srv}

    return render(request, "etat_serveurs.html", context)


def scripts_exec(request, path):
    execute = ScriptThread(path)
    execute.start()
    return render(request, "popup_run.html")


def cout_pli(request):
    reponse = ""
    total = 0
    surtaxe = 0
    poids = 0
    objet_calcul = Calculette()
    with open(os.path.join(BASE_DIR, "./configuration/config_pli.json")) as conf:
        data = json.load(conf)

    tree = ET.parse(os.path.join(BASE_DIR, './configuration/document.xml'))
    element = tree.find('TOTAL/NOMBRE_PLI')

    if request.POST.get('calculer', default=None) is not None:
        nb_pli = int(request.POST.get('nb_plis'))
        element.text = str(nb_pli)
        for entite in data["COGEPRINT"][0]["CONSOMMABLES"]:
            path = 'CONSOMMABLES/' + (entite["FORMAT"] + '/NOMBRE')
            if request.POST.get(entite["FORMAT"]) != "":
                objet_calcul.get(entite["FORMAT"]).setNombre(int(request.POST.get(entite["FORMAT"])))
                tree.find(path).text = request.POST.get(entite["FORMAT"])
            else:
                objet_calcul.get(entite["FORMAT"]).setNombre(0)
                tree.find(path).text = '0'

        if objet_calcul.get("enveloppe_c5").getNombre() == 0 and objet_calcul.get("enveloppe_c4").getNombre() == 0:
            return HttpResponseRedirect('/calculette')
        total = objet_calcul.total_affranchissement(nb_pli)
        poids = objet_calcul.poids_total()
        surtaxe = objet_calcul.surtaxe(nb_pli)
        tree.find('TOTAL/TOTAL_HT').text = str(objet_calcul.total_ht())
        tree.find('TOTAL/TOTAL_TTC').text = str(objet_calcul.total_ttc())
        tree.find('TOTAL/POIDS_TOTAL').text = str(objet_calcul.poids_total())
        tree.find('TOTAL/SURTAXE_PLI').text = str(surtaxe)
        tree.find('TOTAL/TOTAL_FINAL').text = str(total)
        tree.write('./document.xml')
        d = do_sefas_request('./document.xml')
        reponse = d.ask_api()

    tmp = data["COGEPRINT"][0]["CONSOMMABLES"]

    return render(request, "calculette.html",
                  context={"poids": poids, "total": total, "surtaxe": surtaxe, 'data': tmp,
                           'reponse': reponse})


def insert_incident_prod(request):
    conn = connexion_sql()
    global opt, libelle
    tab_options = []
    tab_libelle = []
    try:
        cursor = conn.cursor()

        cursor.execute("SELECT libelle FROM motifs  ORDER BY ID ASC")
        libelle = cursor.fetchall()

        cursor.execute("SELECT ID FROM motifs ORDER BY ID ASC")
        opt = cursor.fetchall()
    except (Exception, ):
        pass

    for row in opt:
        tab_options.append(row[0])

    for row in libelle:
        tab_libelle.append(row[0])

    fusion = {}
    for row in range(len(tab_libelle)):
        fusion[tab_options[row]] = tab_libelle[row]

    if request.POST.get('envoyer', default=None) is not None:

        if request.POST.get('integer', default=None) is not None:
            query = "INSERT INTO incidents_production (DateAno, Solution, Reimpression, motifs_ID ) VALUES (%s, %s, " \
                    "%s,%s) "
            values = (request.POST.get('date'), request.POST.get('solution'), 0,
                      request.POST.get('id_motif'))
        else:
            query = "INSERT INTO incidents_production (DateAno, Solution, Reimpression, motifs_ID ) VALUES (%s, %s, %s,%s)"
            values = (request.POST.get('date'), request.POST.get('solution'), request.POST.get('integer'),
                      request.POST.get('id_motif'))
        cursor.execute(query, values)
        conn.commit()

    return render(request, "incident_prod.html", context={"ids": fusion})


def courrier_pnd(request):
    erreur = ""
    conn = connexion_sql()
    if request.POST.get('envoyer', default=None) is not None:
        id_pnd = request.POST.get('id')
        cursor = conn.cursor()
        try:
            query = "UPDATE usine_courrier SET pnd = 1 WHERE uuidpli = '%s';" % id_pnd
            cursor.execute(query)
            conn.commit()
        except (Exception, ):
            erreur = "ERREUR"
            return render(request, "courrier_pnd.html", context={"erreur": erreur})

    return render(request, "courrier_pnd.html", context={"erreur": erreur})


def insert_cout_editique(request):
    envoye = "envoyé"
    conn = connexion_sql()
    cursor = conn.cursor()

    cursor.execute("SELECT Nom FROM partenaires")
    opt = cursor.fetchall()
    partenaire_tab = []
    for row in opt:
        partenaire_tab.append(row[0])

    type_tab = ["Consommables", "Prestation", "Forfait", "Logiciel", "Matériel"]

    if request.POST.get('envoyer', default=None) is not None:
        if int(request.POST.get('Quantite')) >= 1000:
            prix_au_mil = float(request.POST.get('TotalTTC')) / int(request.POST.get('Quantite')) * 1000
        else:
            prix_au_mil = 0
        query = "INSERT INTO couts_editique (Designation, DateFacturation, Quantite, TotalTTC, PrixMille, PrixU, " \
                "Type, PoidsU, acteurs_Nom ) VALUES (%s, %s, %s,%s, %s, %s, %s,%s,%s) "
        values = (request.POST.get('Designation'), request.POST.get('DateFacturation'), request.POST.get('Quantite'),
                  request.POST.get('TotalTTC'), prix_au_mil,
                  float(request.POST.get('TotalTTC')) / int(request.POST.get('Quantite')),
                  request.POST.get('Type'), request.POST.get('PoidsU'),
                  request.POST.get('partenaires_Nom'))
        cursor.execute(query, values)
        conn.commit()

    return render(request, "cout_editique.html", context={"ids": partenaire_tab, "types": type_tab, "envoye": envoye})


def search_pli(request):
    conn = connexion_sql()
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM table WHERE condition = %s", ('valeur',))

    return render(request, "recherche_pli.html", context={})


def ajout_prod(request):
    envoye = ""
    erreur = ""
    if request.POST.get('envoyer', default=None) is not None:
        conn = connexion_sql()
        cursor = conn.cursor()
        try:
            query = "INSERT INTO production_papier (ID,type, nbplis, nbpages, DateTraitement,DateImpression, cout_total, " \
                    "cout_traitement,cout_affranchissement," \
                    "nbpnd, cout_pnd, acteurs_Nom, ss_type ) VALUES (%s, %s, %s,%s,%s, %s,%s,%s,%s, %s,%s,%s,%s)"
            values = request.POST.get('nom'), request.POST.get('type'), request.POST.get('nbplis'), request.POST.get(
                'nbpages'), \
                     request.POST.get('dateTraitement'), request.POST.get('dateImpression'), \
                     request.POST.get('cout_total'), request.POST.get('cout_traitement'), request.POST.get(
                'cout_affranchissement'), \
                     "0", "0", "ASSURONE", "STD"
            cursor.execute(query, values)
            conn.commit()
            cursor.close()
            envoye = "Envoyé"

        except IntegrityError:
            erreur = "ERREUR: Entrée dupliquée pour le nom (clé primaire)"
            return render(request, "ajout_prod.html", context={"erreur": erreur, "envoye": envoye})
        finally:
            cursor.close()
    return render(request, "ajout_prod.html", context={"erreur": erreur, "envoye": envoye})
