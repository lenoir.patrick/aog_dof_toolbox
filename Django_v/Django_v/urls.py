"""Django_v URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from .views import home, logpaths,upload, search, scripts_run, scripts_exec,cout_pli,insert_incident_prod, courrier_pnd, insert_cout_editique, search_bd, ajout_prod, etat_serveurs
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', scripts_run),
    path("logpaths", logpaths),
    path("search_bd", search_bd),
    path("search", upload),
    path('search_find', search),
    path("production", scripts_run),
    path("calculette", cout_pli),
    path("incident", insert_incident_prod),
    path("pnd", courrier_pnd),
    path("cout_sup", insert_cout_editique),
    path("ajout_prod", ajout_prod),
    path("serveurs", etat_serveurs),
    path("<path:path>",scripts_exec, name="scripts_exec")
]
