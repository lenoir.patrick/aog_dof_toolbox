
import math

from Django_v.page_calcul.entite import Entite


class Calculette:
    enveloppe_c4 = Entite("enveloppe_c4", 0.1, 20)
    enveloppe_c5 = Entite("enveloppe_c5", 0.03, 10)
    papier_cv = Entite("papier_cv", 0.04, 5)
    papier_blanc = Entite("papier_blanc", 0.01, 5)
    porte_vignette = Entite("porte_vignette", 0.49, 86)
    constat_eaux = Entite("constat_eaux", 0.16, 30)
    constat_auto = Entite("constat_auto", 0.1025, 15.41)
    prix_imp = 0.0177
    prix_msp = 0.0368
    TVA = 20
    poids_limite = 35
    aff_c4 = 0.67
    aff_c5 = 0.73
    prix_kg = 10.80

    def poids_total(self):
        return (self.enveloppe_c4.poids_total_entite(self.enveloppe_c4.getNombre()) +
                self.enveloppe_c5.poids_total_entite(self.enveloppe_c5.getNombre()) +
                self.papier_cv.poids_total_entite(self.papier_cv.getNombre()) +
                self.papier_blanc.poids_total_entite(self.papier_blanc.getNombre()) +
                self.porte_vignette.poids_total_entite(self.porte_vignette.getNombre()) +
                self.constat_eaux.poids_total_entite(self.constat_eaux.getNombre()) +
                self.constat_auto.poids_total_entite(self.constat_auto.getNombre()))

    def prix_total(self):
        return (self.enveloppe_c4.prix_total_entite(self.enveloppe_c4.getNombre()) +
                self.enveloppe_c5.prix_total_entite(self.enveloppe_c5.getNombre()) +
                self.papier_cv.prix_total_entite(self.papier_cv.getNombre()) +
                self.papier_blanc.prix_total_entite(self.papier_blanc.getNombre()) +
                self.porte_vignette.prix_total_entite(self.porte_vignette.getNombre()) +
                self.constat_eaux.prix_total_entite(self.constat_eaux.getNombre()) +
                self.constat_auto.prix_total_entite(self.constat_auto.getNombre()))

    def total_imp(self):
        return (self.papier_cv.getNombre() + self.papier_blanc.getNombre()) * self.prix_imp

    def total_msp(self):
        return (self.enveloppe_c4.getNombre() + self.enveloppe_c5.getNombre()) * self.prix_msp

    def frais_fixe(self):
        return self.total_imp() + self.total_msp()

    def total_ht(self):
        return self.prix_total() + self.frais_fixe()

    def total_ttc(self):
        return self.total_ht() + ((self.total_ht() * 20) / 100)

    def total_affranchissement(self, nb_plis):
        if self.poids_total() > self.poids_limite:
            return round(nb_plis * (self.aff_c4 + self.total_ttc()), 2)
        else:
            return round(nb_plis * (self.aff_c5 + self.total_ttc()), 2)

    def surtaxe(self, nb_plis):
        if self.poids_total() > self.poids_limite:
            poids = math.ceil((self.poids_total() * nb_plis) / 1000)
            surtaxe = poids * self.prix_kg
            surtaxe_pli = round(surtaxe / nb_plis, 2)
            prix_pli = round(surtaxe_pli + (self.aff_c4 + self.total_ttc()), 2)
            return round((nb_plis * prix_pli), 2)

    def get(self, name):
        return self.__getattribute__(name)
