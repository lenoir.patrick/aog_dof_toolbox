import os

from requests import request


class do_sefas_request:
    url = 'http://a1-srv-d-edi01:9081/hcs/rest/api/1/json/engines/createEngineSession'
    playload = {'templateName': 'AOG/Scriptlet_Cortex.py',
                'args': ''}
    headers = {
        'Authorization': 'Basic cm9vdDo=',
        'Cookie': 'JSESSIONID=93FD9345067BE198B83CC7D6454A5F62'
    }

    def __init__(self, file):
        self.files = [
            ('binUpLoad', (os.path.basename(file),
                           open(file, 'rb'),
                           'application/octet-stream'))
        ]

    def ask_api(self):
        response = request("POST", self.url, headers=self.headers, data=self.playload, files=self.files)
        return response.text

