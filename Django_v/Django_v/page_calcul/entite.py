class Entite:
    def __init__(self,nom, prix, poids):
        self.nombre = 0
        self.nom = nom
        self.prix = prix
        self.poids = poids

    def getNombre(self):
        return self.nombre

    def setNombre(self, nombre):
        self.nombre = (nombre)

    def poids_total_entite (self, nombre):
        return (nombre) * self.poids

    def prix_total_entite (self, nombre):
        return (nombre) * self.prix