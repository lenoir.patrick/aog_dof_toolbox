"""
This file demonstrates writing tests using the unittest module.
"""

import django
from django.test import TestCase
import os
import sys

"""
When we use Django, we have to tell it which settings we are using. We do this by using an environment variable, DJANGO_SETTINGS_MODULE. 
This is set in manage.py. We need to explicitly set it for tests to work with pytest.
"""

sys.path.append(os.path.join(os.getcwd(), 'Django_v'))
os.environ.setdefault(
    "DJANGO_SETTINGS_MODULE",
    "Django_v.settings"
)
django.setup()

class ViewTest(TestCase):
    """Tests for the application views."""

    if django.VERSION[:2] >= (1, 7):
        # Django 1.7 requires an explicit setup() when running tests in PTVS
        @classmethod
        def setUpClass(cls):
            super(ViewTest, cls).setUpClass()

    def test_unit_home(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'documentation.html')

    def test_unit_logfiles(self):
        response = self.client.get('logpaths')
        self.assertTemplateUsed(response, 'fichier_log.html')  
       
    def test_unit_search(self):
        response = self.client.get('search/')
        self.assertTemplateUsed(response, 'recherche.html')