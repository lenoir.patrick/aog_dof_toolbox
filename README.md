# Introduction
> Ce repository va contenir les librairies de méthodes et classes en commun entre tous les repertoires `AOG-Editique`.
> Pour utiliser et exploiter toutes ces classes, il faut que le chemin d'accès à la racine soit inclus dans le `PATH`
> ou le `PYTHONPATH` de l'environnement executant

# AOG
> Voir le fichier [README.md](./AOG/README.md) associé pour la description des classes et méthodes python

## config.json
> Paramétrage du framework et du comportement des scripts.

| **champ**               | **description**                                                          |
|-------------------------|--------------------------------------------------------------------------|
| **env**                 | Environnement utilisé                                                    |
| **pourcentage_minimum** | Pourcentage minimum d'espace disque disponible pour lancer un traitement |
| **lettre_verte_nombre** | Découpage des lots de production PREVOIR (not used anymore)              |

### mail
> Utilisé par `AOG/Sendmail.py` pour générer les emails de statuts des traitements.

| **champ** | **description**                         |
|-----------|-----------------------------------------|
| **from**  | Expéditeur par défaut du serveur mail   |
| **to**    | Destinataire par défaut du serveur mail |
| **host**  | Adresse hôte du serveur mail            |
| **port**  | Port du serveur mail                    |

### ftp
> Utilisé par `AOG/transferttools.py` pour transférer les fichiers zip pour impression vers PREVOIR.

| **champ**    | **description**                |
|--------------|--------------------------------|
| **protocol** | Protocole de transfert utilisé |
| **login**    | Login de connexion             |
| **password** | Mot de passe de connexion      |
| **address**  | Adresse du serveur ftp         |
| **port**     | Port du serveur ftp            |

### sharepoint
| **champ**   | **description**                   |
|-------------|-----------------------------------|
| **login**   | Login de connexion                |
| **pass**    | Mot de passe de connexion         |
| **sp_site** | Racine du site SharePoint utilisé |
| **tmpdir**  | Répertoire temporaire de travail  |
| **proxy**   | Proxy utilisé pour la connexion   |


### log
| **champ**  | **description**                             |
|------------|---------------------------------------------|
| **logdir** | Répertoire de stockage des fichiers de logs |

### dirs
| **champ**            | **description**                  |
|----------------------|----------------------------------|
| **workingdirectory** | Répertoire de travail par défaut |

### usinecourrier
| **champ**        | **description**                                                            |
|------------------|----------------------------------------------------------------------------|
| **stockdir**     | Répertoire de stockage des fichiers à intégrer                             |
| **active**       | Fonctionnalité d'ajout de Datamatrix activée pour les courriers de gestion |
| **datamatrix_x** | Positionnement x du datamatrix                                             |
| **datamatrix_y** | Positionnement y du datamatrix                                             |


```json
{
    "mail": {
        "from": "editique-ccm@assurone-group.com",
        "to": "editique-ccm@assurone-group.com",
        "host": "10.128.3.250",
        "port": 25
    },
    "ftp": {
        "protocol": "sftp",
        "login": "PrevSgxAss",
        "password": "%40172ER1Ku1eZK",
        "address": "gateway.prevoir.com",
        "port": 10022
    },
    "sharepoint":{
        "login": "plenoir@assurone-group.com",
        "pass": "Fourchett3!",
        "sp_site": "https://assurone.sharepoint.com/sites/DSI/",
        "tmpdir": "D:\\temp",
        "proxy": "http://plenoir@Fourchett3!@10.0.18.15:3128"
    },
    "log": {
        "logdir": "C:\\Users\\plenoir\\Documents\\logs_python"
    },
    "dirs": {
        "workingdirectory": "C:\\Users\\plenoir\\Documents\\DOF_Travail"
    },
    "usinecourrier": {
        "stockdir": "D:\\temp\\stock_usine_courrier",
        "active": true,
        "datamatrix_x": 510,
        "datamatrix_y": 660
    },
    "env": "DEV",
    "pourcentage_minimum": "10",
    "lettre_verte_nombre": 0
}
```

# Django_v
> Voir le fichier [README.md](./Django_v/README.md) associé

# HCS Designer
> Ce répertoire contient les éléments nécessaires à la sauvegarde du référentiel HC Designer. Il n'est donc utile que
> sur le serveur de design, où une tâche planifiée est déjà présente pour automatiser la sauvegarde.

`saveRepository.cmd` => fichier à placer dans HCS/Designer/tools/moconsole

# TF
> Contient les classes spécifiques à l'interconnexion avec la Tech Factory. Il s'agit ici d'un `fork` du dépôt 
`AOG - Editique`.
> La classe principale est `api_request.py` qui va aller appeler l'API éditique pour récupérer les informations nécessaires
> au nommage des fichiers quotidiens avant envoie pour impression.

# tools
> Elements diverses.
> `RAZ_Recette.cmd` Vide tous les répertoires utiles avant de réaliser une recette.