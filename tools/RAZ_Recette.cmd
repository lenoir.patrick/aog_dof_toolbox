@echo off
cls
d:
SET EXT=*.*

SET DIR=D:\travail
call :del_dir %DIR% %EXT%

SET DIR=D:\services\Assurone.Editique.Watcher\App_Data\Logs
call :del_dir %DIR% %EXT%

SET DIR=D:\traitementPostProd\transfertStockage\entree
call :del_dir %DIR% %EXT%

SET DIR=D:\traitementPostProd\transfertStockage\log
call :del_dir %DIR% %EXT%

SET DIR=D:\traitementPostProd\transfertStockage\erreur
call :del_dir %DIR% %EXT%

SET DIR=D:\temp\ 
SET EXT=*.zip
call :del_dir %DIR% %EXT%

SET DIR=D:\temp\stock_usine_courrier\ 
SET EXT=*.ind
call :del_dir %DIR% %EXT%

SET DIR=D:\temp\stock_usine_courrier\ 
SET EXT=*.sql
call :del_dir %DIR% %EXT%

EXIT /B 0


:del_dir
echo Suppression répertoire = %~1 ' / ' %~2
cd %~1
del %~2 /q