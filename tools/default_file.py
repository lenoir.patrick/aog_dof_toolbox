#!python3
# -*- coding: utf-8 -*-
import os
from AOG.tools import GeneralConfig, GeneralError
import AOG.tools as aogtools


""" TEMPLATE DE FICHIER PYTHON A UTILISER """


def sub_name():
    """Méthode globale

    """
    pass


class MyClassException(Exception):
    def __init__(self, ex):
        """Constructeur de la classe
        """
        self.ex = ex

    def __str__(self):
        """Affichage de l'exception
        """
        return "{}: {}".format(type(self.ex).__name__, self.ex)


class MyClass(GeneralConfig):
    def __init__(self, appname):
        """Constructeur

        Args:
            appname: Nom de l'application en cours
        """
        super().__init__(appname)
        self.appname = appname
        self.logwrite.info("Instanciation de la classe : " + self.__class__.__name__)

    def __del__(self):
        """Constructeur de suppression
        """
        pass

    def method(self) -> bool:
        """Méthode

        Args:


        Returns:
            Retour

        Raises:
        """
        pass

    @staticmethod
    def static_method():
        """Méthode statique
        """
        pass


if __name__ == "__main__":
    try:
        appname = os.path.basename(__file__.upper())
        mc = MyClass(appname)
    except Exception as ex:
        ge = mc.logwrite.error(ex)
        ge = GeneralError("ERREUR GENERALE : " + str(ex))
        ge.set_params(appname, mc.log.get_log_path())
        ge.send_error_mail()
        import sys
        sys.exit(-1)

    # Mise à jour de l'écran de supervision <ip>:9100
    aogtools.maj_supervision(appname)
