@echo off
cls

net use Z: /delete /y
net use Z: \\10.128.3.120\cortex coyHbT8cB9WVa! /user:saUsersEditique /persistent:no

set PYTHONHOME=C:\Python310
set PYTHONPATH=D:\scripts\AOG_DOF_Toolbox

set DIR=D:\scripts\transfert

%PYTHONHOME%\Scripts\coverage run -a %DIR%/PREVOIR/gestion_consommables.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/PREVOIR/extractionImpressionJournaliere.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/PREVOIR/TERMES/transfert_termes_sogessur.py

%PYTHONHOME%\Scripts\coverage run -a %DIR%/COGEPRINT/LETTRES_CHEQUE/traitement_lc_cogeprint.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/COGEPRINT/LETTRES_CHEQUE/traitement_pnd_lc_cogeprint.py

%PYTHONHOME%\Scripts\coverage run -a %DIR%/COGEPRINT/RECOMMANDES/envoie_liste_recommandes.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/COGEPRINT/RECOMMANDES/recherche_reco_non_envoyes.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/COGEPRINT/RECOMMANDES/traitement_bdf_recommandes.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/COGEPRINT/RECOMMANDES/traitement_recommandes.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/COGEPRINT/RECOMMANDES/traitement_retour_recommandes.py

%PYTHONHOME%\Scripts\coverage run -a %DIR%/COGEPRINT/TERMES/traitement_pnd_termes.py

%PYTHONHOME%\Scripts\coverage html -d D:\scripts\AOG_DOF_Toolbox\tools\coverage\prestataires

