@echo off
cls

net use Z: /delete /y
net use Z: \\10.128.3.120\cortex coyHbT8cB9WVa! /user:saUsersEditique /persistent:no

set PYTHONHOME=C:\Python310
set PYTHONPATH=D:\scripts\AOG_DOF_Toolbox
set TOOLBOXDIR=%PYTHONPATH%

set DIR=D:\scripts\AOG_DOF_Toolbox\AOG

%PYTHONHOME%\Scripts\coverage run -a %DIR%/constants.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/doc.py %DIR%\doc.py %TOOLBOXDIR%
%PYTHONHOME%\Scripts\coverage run -a %DIR%/hcstools.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/logfile.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/logvolumetries.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/pdftools.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/sendmailtools.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/sharepoint.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/tools.py
%PYTHONHOME%\Scripts\coverage run -a %DIR%/transferttools.py

%PYTHONHOME%\Scripts\coverage html -d D:\scripts\AOG_DOF_Toolbox\tools\coverage\toolbox

