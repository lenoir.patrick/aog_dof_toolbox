@echo off
cls

net use Z: /delete /y
net use Z: \\10.128.3.120\cortex coyHbT8cB9WVa! /user:saUsersEditique /persistent:no

set PYTHONHOME=C:\Python310
set PYTHONPATH=D:\scripts\AOG_DOF_Toolbox

set DIR=D:\scripts\termes_2022
cd D:\scripts\termes_2022
d:

%PYTHONHOME%\Scripts\coverage run -a %DIR%\TermesEditique.py AIG
%PYTHONHOME%\Scripts\coverage run -a %DIR%\TermesEditique.py STD TEST
%PYTHONHOME%\Scripts\coverage run -a %DIR%\TermesEditique.py SANTE TEST
%PYTHONHOME%\Scripts\coverage run -a %DIR%\TermesEditique.py ATTSCO TEST

%PYTHONHOME%\Scripts\coverage html -d D:\scripts\AOG_DOF_Toolbox\tools\coverage\termes


