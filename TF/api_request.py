import json
import os

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

# Disable warning message : “InsecureRequestWarning: Unverified HTTPS request is being made to host python”
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Lecture et convertion du fichier json
path = os.path.dirname(os.path.abspath(__file__))
fileObject = open(os.path.join(path, "apisettings.json"), "r")
jsonContent = fileObject.read()
obj_python = json.loads(jsonContent)


# @Params
#   grant_type, client_ID, client_secret, scope
# @Traitement
#
# @Return
#    retourne le token généré sous forme de chaine de caractères

# edit omar
def get_auth_token(token_url, grant_type, client_id, client_secret, scope):
    # Dictionnaire des paramètres d'indentification à envoyer à l'API
    datas = {
        "grant_type": grant_type,
        "client_ID": client_id,
        "client_secret": client_secret,
        "scope": scope
    }

    # Envoi de la requete POST avec en paramètre l'URL et les paramètres d'identification
    auth_response = requests.post(token_url, data=datas)

    # Extraction de la réponse de la requête au format JSON
    auth_response_json = auth_response.json()

    # Récuperation du token
    auth_token = auth_response_json["access_token"]

    return auth_token


# @Params
#    num_contrat : numero du contrat au format "00000000301"
# @Traitement
#
# @Return
#    retourne les donnees du contrat au format XML


def get_response_xml(num_contrat, type_doc, logger=None):
    # Stockage des valeurs des paramètres d'identification contenues dans le fichier appsettings.json
    token_url = obj_python['token_URL']
    grant_type = obj_python['grant_type']
    client_id = obj_python['client_ID']
    client_secret = obj_python['client_secret']
    scope = obj_python['scope']
    if type_doc == "SIN":
        base_url = obj_python['BASE_URL_SIN']
    else:
        base_url = obj_python['BASE_URL']

    # Formation de l'URL qui sera passée dans la requete GET
    contrat_url = base_url + num_contrat

    # Appel de la fonction de génération du token et stockage du retour de la fonction dans une variable
    auth_token = get_auth_token(token_url, grant_type, client_id, client_secret, scope)

    # Ajout du préfixe Bearer au token
    auth_token_header_value = "Bearer %s" % auth_token

    # Dictionnaire des paramètres à passer au headers, token et format de retour XML
    auth_token_header = {"Authorization": auth_token_header_value, "Accept": "application/xml"}

    try:
        # Envoi de la requête GET pour obtenir les données du contrat
        contrat_response = requests.get(contrat_url, headers=auth_token_header, verify=False)

        # Lève une erreur d'exception si l'appel retourne une erreur de code
        contrat_response.raise_for_status()

        # Extraction des données au format text qui permet un retour au format XML
        donnees_contrat = contrat_response.text

        return donnees_contrat

    except (requests.exceptions.HTTPError, ):
        msg = '/!\ ERROR on num {num}: '.format(num=num_contrat)
        if logger is not None:
            logger.info(msg)
        else:
            print("[red]" + msg)
        return None
    except (requests.exceptions.ProxyError, ):
        msg = 'Proxy non accessible.'
        if logger is not None:
            logger.info(msg)
        else:
            print("[red]" + msg)
        return None


if __name__ == "__main__":
    os.environ["HTTP_PROXY"] = "http://plenoir@Fourchett3!@10.0.18.15:3128"
    print(get_response_xml("CCMA100524", "CP"))
    print(get_response_xml("CCMA100522", "CP"))
