@echo off

rem #####################
rem # Action parameters #
rem #####################
cd D:\HCS\Designer\tools\moconsole
call "setMOConsoleConfig.bat"
D:
set saveDir=D:\HCS\sauvegardes_repository

set type=%1

set dateprod=%date:~6,4%%date:~3,2%%date:~0,2%
set heureProd=%time:~0,2%%time:~3,2%%time:~6,2%

set archiveName=archive_repository_%type%_%dateprod%_%heureProd%.moa
set archiveName=%1
set withFonts=true

rem ###############
rem # Action call #
rem ###############
"%JAVA_HOME%\bin\java" -Xms64m -Xmx256m -DcryptedPassword=%cryptedPassword% -classpath "%class%" com.sefas.service.integration.moconsole.MoConsoleClt SaveRepository %protocol% %serverName% "%saveDir%" "%archiveName%" %withFonts% %MOlogin% "%MOpassword%"

rem pause
 