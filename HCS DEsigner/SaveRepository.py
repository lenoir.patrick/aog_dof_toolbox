#!python
# coding: utf-8

import os
import datetime
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
import smtplib
from email.encoders import encode_base64

import logging
import logging.handlers
import json

LOGLEVELS = {"debug": logging.DEBUG, "info": logging.INFO, "warn": logging.WARNING,
             "warning": logging.WARNING, "error": logging.ERROR}
LEVELNAMES = {50: "CRITICAL", 40: "ERROR", 30: "WARNING", 20: "INFO", 10: "DEBUG", 0: "NOTSET"}

with open(os.path.join(os.path.dirname(__file__), 'appsettings.json')) as json_data:
    config = json.load(json_data)


class logFile:
    def __init__(self):

        now = datetime.datetime.today()
        date = now.strftime("%Y%m%d")
        heure = now.strftime("%H%M%S")

        self._logdir = "D:\\scripts\\log\\"
        self._rootlogname = os.path.basename(__file__).replace(".py", "")
        logname = self._rootlogname + "_" + date + "_" + heure + ".log"
        self._logpath = os.path.join(self._logdir, logname)

    def get_log_path(self):
        return self._logpath

    def delete_old_logs(self, _log):
        nb_save_logs = 0
        for logs in sorted(os.listdir(self._logdir), reverse=True):
            if self._rootlogname in logs:
                if nb_save_logs > 5:
                    os.remove(os.path.join(self._logdir, logs))
                    _log.info("suppression du fichier : " + logs)
                nb_save_logs = nb_save_logs + 1

    def create_logger(self, level='info', console=True):
        """Configure program logger."""
        if level not in LOGLEVELS:
            raise "wrong logging configuration (can be debug|info|warn|error)"
        level = LOGLEVELS[level]
        # This is the "root" logger for CNAM
        _log = logging.getLogger("assurone-group")

        format_str = '%(asctime)-15s [%(levelname)-8s]'

        if level == logging.DEBUG:
            format_str += '%(filename)s:%(lineno)4d (%(funcName)20s) -'
        format_str += ' %(message)s'

        logformatter = logging.Formatter(format_str)
        # CONSOLE LOGGING
        if console:
            streamhandler = logging.StreamHandler()
            streamhandler.setLevel(level)
            streamhandler.setFormatter(logformatter)
            _log.addHandler(streamhandler)
        # FILE LOGGING
        filehandler = logging.handlers.RotatingFileHandler(self._logpath, maxBytes=10485760, backupCount=5)
        filehandler.setLevel(level)
        filehandler.setFormatter(logformatter)
        _log.addHandler(filehandler)
        #
        _log.setLevel(level)
        # 
        _log.debug('Logging initialized to level :'+str(LEVELNAMES[level]))
        #
        self.delete_old_logs(_log)
        return _log


class Sendmail:
    def __init__(self):
        pass


class SaveRepositoryHcs:
    def __init__(self):
        self.dir_save = config['parameters']['dirsave']
        self.scriptdir = config['parameters']['scriptdir']
        self.nbkeepsaves = config['parameters']['nbkeepsaves']

        self.now = datetime.datetime.today()
        self.datejour = self.now.strftime("%Y%m%d")
        self.heure = self.now.strftime("%H%M%S")

        self.log = logFile()
        self.logwrite = self.log.create_logger("debug", True)

    def save_repository(self):
        type_save = ""
        if self.now.weekday() in [0, 1, 2, 3, 4]:  # lundi à vendredi
            type_save = "Q"
            self.logwrite.info("Création d'une archive quotidienne")
        elif self.now.weekday() == 5:  # samedi
            type_save = "H"
            self.logwrite.info("Création d'une archive hebdomadaire")
        archivename = "archive_repository_" + type_save + "_" + self.datejour + "_" + self.heure + ".moa"

        loghcsfile = os.path.join(self.scriptdir, "moconsole.log")
        if os.path.isfile(loghcsfile) is True:
            os.remove(loghcsfile)

        os.chdir(self.scriptdir)
        # Lancement du script de sauvegarde
        cmd = os.path.join(self.scriptdir, "saveRepository.cmd") + " " + archivename
        result = os.system(cmd)

        # On récupère le code erreur de l'executable
        with open(loghcsfile, "r") as logfile:
            for line in logfile:
                line = line.strip()
                if line.startswith("Exit Code :"):
                    result = int(line.replace("Exit Code :", "").strip())

        nb_save_hebdo = 0
        nb_save_quoti = 0
        liste_save_hebdo = []
        liste_save_quoti = []

        # Rotation du nb de sauvegardes si OK
        if result == 0:
            for moa in sorted(os.listdir(self.dir_save), reverse=True):
                if "_Q_" in moa:
                    nb_save_quoti = nb_save_quoti + 1
                    if nb_save_quoti > self.nbkeepsaves:
                        os.remove(os.path.join(self.dir_save, moa))
                        self.logwrite.info("suppression du fichier : " + moa)
                    else:
                        liste_save_quoti.append(moa)
                if "_H_" in moa:
                    nb_save_hebdo = nb_save_hebdo + 1
                    if nb_save_hebdo > self.nbkeepsaves:
                        os.remove(os.path.join(self.dir_save, moa))
                        self.logwrite.info("suppression du fichier : " + moa)
                    else:
                        liste_save_hebdo.append(moa)

        # Preparation du mail
        mail_from = "editique-ccm@assurone-group.com"
        mail_to = "plenoir@assurone-group.com"
        mail_subject = "[EDITIQUE] Sauvegarde du référentiel HCS du " + str(self.datejour)

        mimemsg = MIMEMultipart()
        mimemsg['From'] = mail_from
        mimemsg['To'] = mail_to

        if result == 0:
            self.logwrite.info("La création du moa s'est déroulée correctement")
            mimemsg['X-Priority'] = '4'
            mail_subject = mail_subject + " OK"
            mail_body = "Le traitement s'est déroulé sans erreur.\n"
            mail_body = mail_body + "\n"
            mail_body = mail_body + "La sauvegarde de référentiel a été créé sous : " + \
                        os.path.join(self.dir_save, archivename) + "\n"
            
            mail_body = mail_body + "\n"
            mail_body = mail_body + "Liste des sauvegarde quotidiennes disponibles : \n"
            for item in liste_save_quoti:
                mail_body = mail_body + "  - " + item + " \n"
            mail_body = mail_body + "\n"
            mail_body = mail_body + "Liste des sauvegarde quotidiennes hebdo : \n"
            for item in liste_save_hebdo:
                mail_body = mail_body + "  - " + item + " \n"
        else:
            self.logwrite.error("La création du moa s'est déroulée avec erreur")
            mimemsg['X-Priority'] = '2'
            mail_subject = mail_subject + " KO"
            mail_body = "Le traitement s'est déroulé avec erreur.\n"
            mail_body = mail_body + "\n"
            mail_body = mail_body + "Merci de regarder le script de sauvegarde manuellement\n"

            part = MIMEBase('application', "octet-stream")
            logfile = self.log.get_log_path()
            part.set_payload(open(logfile, "rb").read())
            encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(logfile))
            mimemsg.attach(part)
            try:
                part = MIMEBase('application', "octet-stream")
                logfile = self.log.get_log_path()
                part.set_payload(open(logfile, "rb").read())
                encode_base64(part)
                part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(logfile))
                mimemsg.attach(part)
            except (Exception, ):
                mail_body = mail_body + "Le fichier moconsole.log n'a pas pu être récupéré\n"

        mail_body = mail_body + "\n"
        mail_body = mail_body + "L'équipe Editique"
        self.logwrite.error("Préparation du mail de retour")
        try:
            mimemsg['Subject'] = mail_subject
            mimemsg.attach(MIMEText(mail_body, 'plain'))
            connection = smtplib.SMTP("10.128.3.250", port=25)
            connection.starttls()

            connection.send_message(mimemsg)
            connection.quit()
            self.logwrite.info("L'email envoyé avec succès")
        except Exception as ex:
            print(str(ex))
            self.logwrite.error("L'email n'a pas pu être envouyé pour raison : " + str(ex))


if __name__ == "__main__":
    sr = SaveRepositoryHcs()

    sr.save_repository()
